  // ------------------------------------------------------ //
  // ------------------ FAQ TOGGLE BUTTON ----------------- //
  // ------------------------------------------------------ //

  let toggleClosed = true;
  const toggleButton = document.querySelector('.js--toggle-button');
  const accordionItems = document.querySelectorAll('.js--accordion-item');
  const collapsedAccordions = document.querySelectorAll('.js--accordion-collapse');

  if(toggleButton) {
    toggleButton.addEventListener('click', (event) => {
      event.preventDefault();
      if(toggleClosed) {
        toggleClosed = false;
        accordionItems.forEach(function(accordion) {
          if(accordion.getAttribute('aria-expanded') == 'false') {
            accordion.classList.remove('.collapsed');
            accordion.setAttribute('aria-expanded', 'true');
            // toggleButton.style.marginLeft = '25px';
            toggleButton.classList.add('on')
            collapsedAccordions.forEach(function(collapsedAccordion) {
              collapsedAccordion.classList.add('show');
            })
          }
        })
      } else {
        toggleClosed = true;
        accordionItems.forEach(function(accordion) {
          if(accordion.getAttribute('aria-expanded') == 'true') {
            accordion.classList.add('.collapsed');
            accordion.setAttribute('aria-expanded', 'false');
            // toggleButton.style.marginLeft = '3px';
            toggleButton.classList.remove('on')
            collapsedAccordions.forEach(function(collapsedAccordion) {
              collapsedAccordion.classList.remove('show');
            })
          }
        })
      }
    });
  }
