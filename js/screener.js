//Disable scroll for screener
(function() {
  function receiveMessage(e) {
    var iframe = document.getElementById('qualify-iframe');
    if (iframe) {
      var src = iframe.src;
      if ( src && src.indexOf( e.origin ) === 0 ) {
        var data = JSON.parse(e.data);
        switch ( data.event ) {
          case 'size':
          iframe.style.height = (data.height)+'px';
      		if (data.scrollToTop) {
						document.getElementById("qualify-iframe").scrollIntoView(false);
      		}
        	break;   
      	}
      }
    }
  }
  
  if ( window.addEventListener ) {
    window.addEventListener('message', receiveMessage, false);
  } else {
    window.attachEvent('onmessage', receiveMessage);
  }
})();

// //Preloader for screener
// const screener = document.getElementById("screener");
// const screenerLoading = document.getElementById("screener-loading");

// if (screener) {
//   setTimeout(function () {
//     screener.style.display = "block";
//     screenerLoading.style.display = "none";
//   }, 5000);
// }
