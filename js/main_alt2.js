// Document load/ready promise, calls 'ready' method when completed
HTMLDocument.prototype.ready = function () {
  return new Promise(function(resolve, reject) {
    if (document.readyState === 'complete') {
      resolve(document);
    } else {
      document.addEventListener('DOMContentLoaded', function() {
        resolve(document);
      });
    }
  });
};

document.ready().then(function() {

  $('.external-link').on('click', function (e) {
    e.preventDefault();
    // console.log($(e.currentTarget).attr('href'));
    let externalLink = $(e.currentTarget).attr('href');
    // console.log(externalLink);
    // document.getElementById( "btnContinue" ).setAttribute( "onClick", (       "window.location.href=\"" + $(e.currentTarget).attr('href') + "\"") );
    const btn = document.getElementById('btnContinue')
    btn.addEventListener('click', function (e) {
      e.preventDefault()
      window.open(externalLink, '_blank')
    });
    $('#outbound-modal').modal('show');
  });

  $('.external-link2').on('click', function (e) {
    e.preventDefault();
    // console.log($(e.currentTarget).attr('href'));
    let externalLink = $(e.currentTarget).attr('href');
    // console.log(externalLink);
    // document.getElementById( "btnContinue" ).setAttribute( "onClick", (       "window.location.href=\"" + $(e.currentTarget).attr('href') + "\"") );
    const btn = document.getElementById('btnContinue2')
    btn.addEventListener('click', function (e) {
      e.preventDefault()
      window.open(externalLink, '_blank')
    });

    $('#outbound-modal2').modal('show');
  });

  // ------------------------------------------------------ //
  // -------------- JUMBOTRON IMAGE SELECTOR -------------- //
  // ------------------------------------------------------ //
  const heroJumbotron = document.querySelector('.js--hero-jumbotron');
  const heroText = document.querySelector('.js--hero-text');
  const heroVerbiage = {
    0: 'Does she know IBD?',
    1: 'Does he know IBD?'
    // 2: 'Does he know IBD?'
  };
  const heroImagePaths = {
    0: 'images/front-hero-new-0.jpg',
    1: 'images/front-hero-new-1.jpg'
    // 2: 'images/front-hero-new-2.jpg',
  }

  if (heroJumbotron) {
    preLoadHeroImages();
    // homeHero(0);

    let i = 0;
    setInterval(() => {
      i += 1;
      if (i >= Object.keys(heroImagePaths).length) i = 0;
      homeHero(i);
    } , 5000);
  }

  function preLoadHeroImages() {
    for (let i = 0; i < Object.keys(heroImagePaths).length; i += 1) {
      const tempImage = new Image();
      tempImage.src = heroImagePaths[i];
    };
  }

  function homeHero(number) {
    heroJumbotron.style.opacity = 0;

    // allow for opacity transition to complete
    setTimeout(() => {
      heroJumbotron.style.backgroundImage = `url(${heroImagePaths[number]})`;
      heroText.innerHTML = heroVerbiage[number];
      heroJumbotron.style.opacity = 1;
    }, 350);
  }

  // ------------------------------------------------------ //
  // ------------ ARROW BUTTON SECTION SNAPPING ----------- //
  // ------------------------------------------------------ //

  // Outer sections to scroll/snap to
  const scrollSections = document.querySelectorAll('[data-scroll-section]');

  // Buttons/elements to trigger scroll/snap
  const scrollButtons = document.querySelectorAll('[data-scroll-button]');

  // Sections to scroll/snap to, including outer/inner sections
  const allScrollSections = document.querySelectorAll('[data-scroll-section], [data-scroll-section-inner]');

  if ( scrollSections.length > 0 && scrollButtons.length > 0 && allScrollSections.length > 0 ) {
    for(const button of scrollButtons) {

      // Find the parent section for this button
      const section = button.closest('[data-scroll-section]');

      // Find the section index within the sections node list
      const index = Array.prototype.indexOf.call(scrollSections, section);

      // Sections inside a scrollable element, which is inside of an outer section
      const innerSections = section.querySelectorAll('[data-scroll-section-inner]');

      let innerSectionsParent = null;

      if(innerSections && innerSections.length > 0) {
        // The scrollable element that contains the inner sections
        innerSectionsParent = innerSections[0].closest('[data-scroll-section-inner-parent]');
      }


      button.addEventListener('click', event => {
        event.preventDefault();

        // No inner sections, so scroll to next outer section if available
        if(!innerSections || innerSections.length < 1) {
          // Scroll to the next outter section if it exists
          scrollToNextSection(scrollSections, index);
        }
        // There are inner sections
        else {
          let innerIndex = 0;

          // Find the currently visible inner section and its index
          for(const inner of innerSections) {
            if(isInViewport(inner)) {
              innerIndex = Array.prototype.indexOf.call(innerSections, inner);
              break;
            }
          }

          // There is an inner section after the currently visible one
          if(innerSections[innerIndex+1]) {
            const offset = innerSections[innerIndex+1].offsetTop - innerSectionsParent.offsetTop;

            // Scroll to the top of the next inner section, within the scrollable section
            innerSectionsParent.scroll({
              top: offset,
              left: 0,
              behavior: 'smooth',
            });
          }
          // No other inner section
          else {
            // Scroll to the next outter section if it exists
            scrollToNextSection(scrollSections, index);
          }

        }
      });
    }

    // ------------------------------------------------------ //
    // --------------- SCROLL SECTION SNAPPING -------------- //
    // ------------------------------------------------------ //

    let scrollSnappingIndex = 0;
    let scrollSnappingIsRunning = false;

    document.body.addEventListener('wheel', event => {
      event.preventDefault();
      event.stopPropagation();

      if(!scrollSnappingIsRunning && allScrollSections[scrollSnappingIndex] != undefined) {
        scrollSnappingIsRunning = true;

        // Scrolled down
        if(event.deltaY > 0) {
          scrollSnappingIndex += 1;

          if(scrollSnappingIndex >= allScrollSections.length) {
            scrollSnappingIndex = allScrollSections.length - 1;
          }
        }
        // Scrolled up
        else {
          scrollSnappingIndex -= 1;

          if(scrollSnappingIndex <= 0) {
            scrollSnappingIndex = 0;
          }
        }

        // Scroll to the section
        allScrollSections[scrollSnappingIndex].scrollIntoView({
          behavior: 'smooth',
        });

        setTimeout(() => {
          scrollSnappingIsRunning = false;
        }, 500);

      }
    }, { passive: false });
  }

  // ------------------------------------------------------ //
  // ------------------------ SLIDER ---------------------- //
  // ------------------------------------------------------ //

  if ( $('.js--slider').length > 0 ) {
    let deg = 0;
    $('.js--slider').slick({
      arrows: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      vertical: true,
      dots: true,
      speed: 600,
      cssEase: 'cubic-bezier(0.390, 0.575, 0.565, 1.000)',
      customPaging : function(slider, i) {
        const years = {
          "0": "1859",
          "1": "1955",
          "2": "1975",
          "3": "1998",
          "4": "1998",
          "5": "2006",
          "6": "2011",
          "7": "RECENT",
          "8": "NOW"
        };
        return '<a class="tk-roc-grotesk-condensed timeline__nav__item js--nav-item">' + years[i] + '<div class="dot dot--purple"></div></a>';
      },
      appendDots: $('.js--slider-nav'),
    }).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
      const year = $(slick.$slides.get(nextSlide)).data('year');
      const yearText = $(slick.$slides.get(nextSlide)).data('yearText');

      if ( year != undefined ) {
        $('.js--img-circle-year')
          .fadeOut(500, function() {
            $('.js--img-circle-year').html(year).fadeIn(400);
            if ( yearText != undefined ) {
              $('.js--img-circle-year-text').html(yearText).fadeIn(400);
            } else {
              $('.js--img-circle-year-text').html(yearText).fadeOut(400);
            }
          });
      }

      $('.js--slider-nav .slick-dots .slick-active').next().find('.dot').css({
        "background-color": "#F36A2E",
        "transition": "0.6s ease-in-out 0.1s"
      });

      $('.js--slider-nav .slick-dots .slick-active').nextAll().find('.dot').css({
        "background-color": "#8670B3",
        "transition": "0.6s ease-in-out"
      });

      $('.js--slider-nav .slick-dots .slick-active').prevAll().find('.dot').css({
        "background-color": "#F36A2E",
        "transition": "0.6s ease-in-out"
      });

      nextSlide = (nextSlide / 8) * 100;

      $('.js--timeline-progress-line').css({
        "width": nextSlide + "%"
      });
    }).on('afterChange', function(event, slick, currentSlide, nextSlide) {
      $('.js--slider-nav .slick-dots .slick-active').find('.dot').css({
        "background-color": "#F36A2E",
        "transition": "0.6s ease-in-out 0.1s"
      });

      $('.js--slider-nav .slick-dots .slick-active').nextAll().find('.dot').css({
        "background-color": "#8670B3",
        "transition": "0.6s ease-in-out"
      });

      $('.js--slider-nav .slick-dots .slick-active').prevAll().find('.dot').css({
        "background-color": "#F36A2E",
        "transition": "0.6s ease-in-out"
      });
    });

    $('.js--slider-prev').click(function(e) {
      e.preventDefault();
      $(this).parent().parent().find('.slick-slider').slick('slickPrev');

      deg -= 10;

      $('.js--img-circle').css({
        "transform": "rotate(" + Math.abs(deg) + "deg)"
      });
    });

    $('.js--slider-next').click(function(e) {
      e.preventDefault();
      $(this).parent().parent().parent().parent().parent().find('.slick-slider').slick('slickNext');

      deg += 10;

      $('.js--img-circle').css({
        "transform": "rotate(-" + Math.abs(deg) + "deg)"
      });
    });

    function recalculateSliderHeight() {
      const slides = $('.js--timeline-slide');
      console.log(slides)
      // Reset
      for ( const slide of slides ) {
        slide.style.height = 'auto'
      }

      // Calculate
      const highest = Math.max(...slides.map((index, component) => component.getBoundingClientRect().height))

      // Apply
      for ( const newSlide of slides ) {
        newSlide.style.height = highest + 15 + 'px'
      }
    };
    recalculateSliderHeight();
  }

  // ------------------------------------------------------ //
  // ------------------ OUTBOUND MODAL -------------------- //
  // ------------------------------------------------------ //

  const outboundLinks = document.querySelectorAll('.js--outbound-link');
  const outboundContinueEl = document.querySelector('.js--outbound-continue');


  outboundLinks.forEach((outboundLink) => {
    outboundLink.addEventListener('click', (event) => {
      event.preventDefault();
      outboundContinueEl.href = outboundLink.href;
    })
  });

  $(".modal a").click(function(){
    $(this).closest(".modal").modal("hide");
  });


  // ------------------------------------------------------ //
  // ----------- Participating Countries - Scroll --------- //
  // ------------------------------------------------------ //

  $(function() {
    $(".wrapper1").scroll(function(){
      $(".wrapper2").scrollLeft($(".wrapper1").scrollLeft());
    });
    $(".wrapper2").scroll(function(){
      $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft());
    });
  });

  // ------------------------------------------------------ //
  // -------------------- NAV DROPDOWNS ------------------- //
  // ------------------------------------------------------ //

  const dropdownNavItems = document.querySelectorAll('.js--dropdown-arrow');
  dropdownNavItems.forEach(function(dropdownNavItem) {
    dropdownNavItem.addEventListener('click', (event) => {
      if ( window.innerWidth < 992 ) {
        event.preventDefault();
      }
    })
  })
});



function preventScroll(e) {
  e.preventDefault();
  e.stopPropagation();

  return false;
}

function isElementXPercentInViewport(el, percentVisible) {
  let
    rect = el.getBoundingClientRect(),
    windowHeight = (window.innerHeight || document.documentElement.clientHeight);

  return !(
    Math.floor(100 - (((rect.top >= 0 ? 0 : rect.top) / +-rect.height) * 100)) < percentVisible ||
    Math.floor(100 - ((rect.bottom - windowHeight) / rect.height) * 100) < percentVisible
  )
};


function scrollToPreviousSection(sections, currentIndex) {

  if(sections[currentIndex-1]) {
    // Smooth scroll to the next section
    sections[currentIndex-1].scrollIntoView({
      behavior: 'smooth',
    });
  }

}


function scrollToNextSection(sections, currentIndex) {

  if(sections[currentIndex+1]) {
    // Smooth scroll to the next section
    sections[currentIndex+1].scrollIntoView({
      behavior: 'smooth',
    });
  }

}

function isInViewport(element) {
    const rect = element.getBoundingClientRect();
    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

document.addEventListener("DOMContentLoaded", function() {
  window.addEventListener('scroll', function() {
    if (window.scrollY > 20 && window.innerWidth > 1200) {
      document.body.classList.add('fixed-top-body');
      document.getElementById('navbar').classList.add('fixed-top');
      // add padding top to show content behind navbar
      navbar_height = document.querySelector('.navbar').offsetHeight;
      document.body.style.paddingTop = navbar_height + 'px';
    } else {
      document.getElementById('navbar').classList.remove('fixed-top');
       // remove padding top from body
      document.body.style.paddingTop = '0';
    }
  });
});
