var map;
var markers = [];
var infoWindow;
let locations = [
  // {
  //   "id": "2",
  //   "name": "Cancer Specialists of North Florida",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 30.2503941,
  //     "longitude": -81.5819672
  //   },
  //   "address": {
  //     "streetAddressLine1": "7015 A C Skinner Pkwy",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Jacksonville",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "32256"
  //   },
  // },
  // {
  //   "id": "3",
  //   "name": "Yuma Regional Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.6843801,
  //     "longitude": -114.638309
  //   },
  //   "address": {
  //     "streetAddressLine1": "2375 S. Ridgeview Dr",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Yuma",
  //     "state": "AZ",
  //     "countryCode": "US",
  //     "postalCode": "85364"
  //   },
  // },
  // {
  //   "id": "4",
  //   "name": "University Cancer & Blood Center, LLC",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.98468,
  //     "longitude": -83.4164411
  //   },
  //   "address": {
  //     "streetAddressLine1": "3320 Old Jefferson Rd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Athens",
  //     "state": "GA",
  //     "countryCode": "US",
  //     "postalCode": "30607"
  //   },
  // },
  // {
  //   "id": "5",
  //   "name": "Astera Cancer Care",
  //   "phoneNumber": null,
  //   "clinicType": "multiple",
  //   "clinicTypes": "MARIPOSA & MARIPOSA 2 & PAPILLON",
  //   "coordinates": {
  //     "latitude": 40.4095775,
  //     "longitude": -74.4252833
  //   },
  //   "address": {
  //     "streetAddressLine1": "j2 Brier Hill Court",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "East Brunswick",
  //     "state": "NJ",
  //     "countryCode": "US",
  //     "postalCode": "08816"
  //   },
  // },
  // {
  //   "id": "6",
  //   "name": "Pan American Center for Oncology Trials LLC",
  //   "phoneNumber": null,
  //   "clinicType": "multiple",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 18.3954164,
  //     "longitude": -66.0727906,
  //   },
  //   "address": {
  //     "streetAddressLine1": "Hospital Oncológico Centro Medico Bo Monacillos #150",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "San Juan",
  //     "state": "PR",
  //     "countryCode": "US",
  //     "postalCode": "00935"
  //   },
  // },
  // {
  //   "id": "7",
  //   "name": "City of Hope Long Beach Elm. ",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.7799411,
  //     "longitude": -118.1882347,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1043 Elm Ave.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Long Beach",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90813"
  //   },
  // },
  // {
  //   "id": "9",
  //   "name": "Sarah Cannon Research Institute",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 36.1528955,
  //     "longitude": -86.8141073,
  //   },
  //   "address": {
  //     "streetAddressLine1": "250 25th Avenue North",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Nashville",
  //     "state": "TN",
  //     "countryCode": "US",
  //     "postalCode": "37203"
  //   },
  // },
  {
    "id": "11",
    "name": "Washington University School of Medicine",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.6348045,
      "longitude": -90.2653954,
    },
    "address": {
      "streetAddressLine1": "660 South Euclid Avenue",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "St. Louis",
      "state": "MO",
      "countryCode": "US",
      "postalCode": "63110"
    },
  },
  // {
  //   "id": "12",
  //   "name": "Henry Ford Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.3674808,
  //     "longitude": -83.0885851,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2799 W Grand Blvd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Detroit",
  //     "state": "MI",
  //     "countryCode": "US",
  //     "postalCode": "48202"
  //   },
  // },
  {
    "id": "13",
    "name": "Minnesota Oncology Hematology, P.A.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 45.1072442,
      "longitude": -93.2613722,
    },
    "address": {
      "streetAddressLine1": "480 Osborne Rd NE",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Fridley",
      "state": "MN",
      "countryCode": "US",
      "postalCode": "55432"
    },
  },
  // {
  //   "id": "15",
  //   "name": "Mayo Clinic",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 44.022597,
  //     "longitude": -92.4689573,
  //   },
  //   "address": {
  //     "streetAddressLine1": "200 First Street Sw",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Rochester",
  //     "state": "MN",
  //     "countryCode": "US",
  //     "postalCode": "55905"
  //   },
  // },
  // {
  //   "id": "16",
  //   "name": "H. Lee Moffitt Cancer & Research Institute",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.0641139,
  //     "longitude": -82.4232426,
  //   },
  //   "address": {
  //     "streetAddressLine1": "12902 Usf Magnolia Dr",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Tampa",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33612"
  //   },
  // },
  // {
  //   "id": "17",
  //   "name": "Columbia University Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.8412426,
  //     "longitude": -73.9431646,
  //   },
  //   "address": {
  //     "streetAddressLine1": "622 W 168th St",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10032"
  //   },
  // },
  // {
  //   "id": "18",
  //   "name": "City of Hope Comprehensive Cancer Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.1291643,
  //     "longitude": -117.9747782,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1500 East Duarte Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Duarte",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "91010"
  //   },
  // },
  // {
  //   "id": "19",
  //   "name": "East Jefferson General Hospital",
  //   "clinicType": "mariposa",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 30.0144319,
  //     "longitude": -90.1814771
  //    ,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4204 Houma Boulevard",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Metairie",
  //     "state": "LA",
  //     "countryCode": "US",
  //     "postalCode": "70006"
  //   },
  // },
  // {
  //   "id": "20",
  //   "name": "David Geffen School of Medicine at UCLA",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.0627642,
  //     "longitude": -118.4481808,
  //   },
  //   "address": {
  //     "streetAddressLine1": "10911 Weyburn Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Los Angeles",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90095"
  //   },
  // },
  // {
  //   "id": "21",
  //   "name": "Columbia University Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.8407787,
  //     "longitude": -73.9431972,
  //   },
  //   "address": {
  //     "streetAddressLine1": "161 Fort Washington Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10032"
  //   },
  // },
  // {
  //   "id": "22",
  //   "name": "Providence Portland Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.5276578,
  //     "longitude": -122.6125753,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4805 NE Glisan Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Portland",
  //     "state": "OR",
  //     "countryCode": "US",
  //     "postalCode": "97213"
  //   },
  // },
  // {
  //   "id": "23",
  //   "name": "University of California, Irvine",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.7870168,
  //     "longitude": -117.8912669,
  //   },
  //   "address": {
  //     "streetAddressLine1": "101 The City Drive South",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Orange",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92868"
  //   },
  // },
  // {
  //   "id": "24",
  //   "name": "University of California Irvine",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.6744215,
  //     "longitude": -117.830078,
  //   },
  //   "address": {
  //     "streetAddressLine1": "200 Manchester Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Orange",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92868"
  //   },
  // },
  // {
  //   "id": "25",
  //   "name": "Utah Cancer Specialists",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.6877461,
  //     "longitude": -111.8725512,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3838 South 700 East Suite 100",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Salt Lake City",
  //     "state": "UT",
  //     "countryCode": "US",
  //     "postalCode": "84106"
  //   },
  // },
  // {
  //   "id": "26",
  //   "name": "Sanford Health",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.5373195,
  //     "longitude": -96.7437169,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1309 W 17th St",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Sioux Falls",
  //     "state": "SD",
  //     "countryCode": "US",
  //     "postalCode": "57104"
  //   },
  // },
  // {
  //   "id": "27",
  //   "name": "American Oncology Partners of Maryland, PA",
  //   "phoneNumber": null,
  //   "clinicType": "chrysalis",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.0239646,
  //     "longitude": -77.1336856,
  //   },
  //   "address": {
  //     "streetAddressLine1": "6410 Rockledge Drive",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Bethesda",
  //     "state": "MD",
  //     "countryCode": "US",
  //     "postalCode": "20817"
  //   },
  // },
  // {
  //   "id": "28",
  //   "name": "Oncology Consultants",
  //   "phoneNumber": null,
  //   "clinicType": "chrysalis",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 29.7064942,
  //     "longitude": -95.4101911,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2130 W Holcombe Blvd, 10th Floor",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Houston",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "77030"
  //   },
  // },
  // {
  //   "id": "29",
  //   "name": "Boston Medical College",
  //   "phoneNumber": null,
  //   "clinicType": "chrysalis-2",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.3351149,
  //     "longitude": -71.0755736,
  //   },
  //   "address": {
  //     "streetAddressLine1": "830 Harrison Avenue, 3rd Floor",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Boston",
  //     "state": "MA",
  //     "countryCode": "US",
  //     "postalCode": "02118"
  //   },
  // },
  // {
  //   "id": "30",
  //   "name": "Cedars Sinai Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.0750241,
  //     "longitude": -118.3827434,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8700 Beverly Boulevard",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Los Angeles",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90048"
  //   },
  // },
  // {
  //   "id": "31",
  //   "name": "Columbia University Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "papillon",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.8407785,
  //     "longitude": -73.9453867,
  //   },
  //   "address": {
  //     "streetAddressLine1": "161 Fort Washington Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10032"
  //   },
  // },
  // {
  //   "id": "32",
  //   "name": "Dana Farber",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.3374515,
  //     "longitude": -71.1103686,
  //   },
  //   "address": {
  //     "streetAddressLine1": "450 Brookline Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Boston",
  //     "state": "MA",
  //     "countryCode": "US",
  //     "postalCode": "02215"
  //   },
  // },
  // {
  //   "id": "33",
  //   "name": "Hartford Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.6747839,
  //     "longitude": -72.824543,
  //   },
  //   "address": {
  //     "streetAddressLine1": "201 N Mountain Rd.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Plainville",
  //     "state": "CT",
  //     "countryCode": "US",
  //     "postalCode": "06062"
  //   },
  // },
  // {
  //   "id": "34",
  //   "name": "Icahn School of Medicine at Mt. Sinai",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.7900629,
  //     "longitude": -73.9541332,
  //   },
  //   "address": {
  //     "streetAddressLine1": "One Gusatve L Levy Place",
  //     "streetAddressLine2": " Box 1132",
  //     "streetAddressLine3": null,
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10029"
  //   },
  // },
  // {
  //   "id": "35",
  //   "name": "Kaiser Permanente Northwest",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.5431632,
  //     "longitude": -122.6793148,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2800 N Interstate Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Portland",
  //     "state": "OR",
  //     "countryCode": "US",
  //     "postalCode": "97227"
  //   },
  // },
  // {
  //   "id": "36",
  //   "name": "Langone Health at NYC University, NYU School of Medicine",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.745759,
  //     "longitude": -73.9809485,
  //   },
  //   "address": {
  //     "streetAddressLine1": "160 East 34th Street",
  //     "streetAddressLine2": "8th Floor",
  //     "streetAddressLine3": "Suite 840",
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10016"
  //   },
  // },
  // {
  //   "id": "37",
  //   "name": "Long Island Jewish Medical Center (Northwell Health Cancer Institute)",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.7579929,
  //     "longitude": -73.7021398,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1111 Marcus Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Lake Success",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "11042"
  //   },
  // },
  // {
  //   "id": "38",
  //   "name": "Mass General",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.3623636,
  //     "longitude": -71.0721664,
  //   },
  //   "address": {
  //     "streetAddressLine1": "55 Fruit Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Boston",
  //     "state": "MA",
  //     "countryCode": "US",
  //     "postalCode": "02114"
  //   },
  // },
  {
    "id": "39",
    "name": "MUSC-Hollings Cancer Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.783438,
      "longitude": -79.9500137,
    },
    "address": {
      "streetAddressLine1": "86 Jonathan Lucas St, MSC 955",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Charleston",
      "state": "SC",
      "countryCode": "US",
      "postalCode": "02942"
    },
  },
  // {
  //   "id": "40",
  //   "name": "Samuel Oschin Comprehensive Cancer Centers Cedars-Sinai Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.0761098,
  //     "longitude": -118.3821879,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8700 Beverly Blvd.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "West Hollywood",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90048"
  //   },
  // },
  // {
  //   "id": "41",
  //   "name": "Stanford",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 37.4357319,
  //     "longitude": -122.1784753,
  //   },
  //   "address": {
  //     "streetAddressLine1": "875 Blake Wilbur Dr. ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Stanford",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "94305"
  //   },
  // },
  // {
  //   "id": "42",
  //   "name": "TriHealth Network",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.1399734,
  //     "longitude": -84.5241361,
  //   },
  //   "address": {
  //     "streetAddressLine1": "375 Dixmyth Ave. 4th Floor",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Cincinnati",
  //     "state": "OH",
  //     "countryCode": "US",
  //     "postalCode": "45220"
  //   },
  // },
  // {
  //   "id": "43",
  //   "name": "UC Irvine",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.7870682,
  //     "longitude": -117.8922178,
  //   },
  //   "address": {
  //     "streetAddressLine1": "101 The City Drive South Bldg 200, 4th floor",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Orange",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92868"
  //   },
  // },
  // {
  //   "id": "44",
  //   "name": "University of California, San Francisco",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 37.7675502,
  //     "longitude": -122.392569,
  //   },
  //   "address": {
  //     "streetAddressLine1": "550 16th Street, 6th Floor",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "San Francisco",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "94158"
  //   },
  // },
  // {
  //   "id": "45",
  //   "name": "University of Chicago Comprehensive Cancer Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.7885181,
  //     "longitude": -87.6054553,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5841 S. Maryland Ave MC 2115",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Chicago",
  //     "state": "IL",
  //     "countryCode": "US",
  //     "postalCode": "60637"
  //   },
  // },
  // {
  //   "id": "46",
  //   "name": "University of Mississippi",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.3293793,
  //     "longitude": -90.1970606,
  //   },
  //   "address": {
  //     "streetAddressLine1": "350 West Woodrow Wilson",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Jackson",
  //     "state": "MS",
  //     "countryCode": "US",
  //     "postalCode": "39213"
  //   },
  // },
  {
    "id": "47",
    "name": "University of Pennsylvania Division of Hematology Oncology Perelman Center for Advanced Medicine ",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.9476279,
      "longitude": -75.1947867,
    },
    "address": {
      "streetAddressLine1": "South Pavillion, Floor 10 Office# 10-204 3400 Civic Center Blvd ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Philadelphia",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "19104"
    },
  },
  // {
  //   "id": "48",
  //   "name": "University of Pittsburgh Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.4545985,
  //     "longitude": -79.9439292,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5150 Centre Avenue Suite 301",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Pittsburgh",
  //     "state": "PA",
  //     "countryCode": "US",
  //     "postalCode": "15232"
  //   },
  // },
  // {
  //   "id": "49",
  //   "name": "University of Tennessee Health Science Centre",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.142258,
  //     "longitude": -90.0337481,
  //   },
  //   "address": {
  //     "streetAddressLine1": "877 Jefferson Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Memphis",
  //     "state": "TN",
  //     "countryCode": "US",
  //     "postalCode": "38103"
  //   },
  // },
  // {
  //   "id": "50",
  //   "name": "University of Vermont Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 44.5099686,
  //     "longitude": -73.0378601,
  //   },
  //   "address": {
  //     "streetAddressLine1": "111 Colchester Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Burlington",
  //     "state": "VT",
  //     "countryCode": "US",
  //     "postalCode": "05401"
  //   },
  // },
  // {
  //   "id": "51",
  //   "name": "University of Virginia",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa-2",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 38.0320013,
  //     "longitude": -78.5008744,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1215 Lee Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Charlottesville",
  //     "state": "VA",
  //     "countryCode": "US",
  //     "postalCode": "22903"
  //   },
  // },
  // {
  //   "id": "52",
  //   "name": "USC Norris Comprehensive Cancer Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.0615104,
  //     "longitude": -118.2075309,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1441 Eastlake Avenue, NTT 3440",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Los Angeles",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90033"
  //   },
  // },
  {
    "id": "53",
    "name": "Veteran Affairs Sierra Nevada Health Care System",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.5171226,
      "longitude": -119.8014454,
    },
    "address": {
      "streetAddressLine1": "975 Kirman Ave.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Reno",
      "state": "NV",
      "countryCode": "US",
      "postalCode": "89502"
    },
  },
  // {
  //   "id": "54",
  //   "name": "Virginia Cancer Specialists",
  //   "phoneNumber": null,
  //   "clinicType": "multiple",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 38.8723521,
  //     "longitude": -77.2444103,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8613 Lee Highway",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Fairfax",
  //     "state": "VA",
  //     "countryCode": "US",
  //     "postalCode": "22031"
  //   },
  // },
  {
    "id": "55",
    "name": "MAYO in Rochester",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 44.022597,
      "longitude": -92.468952,
    },
    "address": {
      "streetAddressLine1": "200 First Street SW",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Rochester",
      "state": "MN",
      "countryCode": "US",
      "postalCode": "55905"
    },
  },
  // {
  //   "id": "57",
  //   "name": "Holy Cross Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "mariposa-2",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 38.0320013,
  //     "longitude": -78.5008744,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4725 N Federal Highway",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Fort Lauderdale",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33308"
  //   },
  // },
  // {
  //   "id": "58",
  //   "name": "A1 clinical Network",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.700460,
  //     "longitude": -73.835900,
  //   },
  //   "address": {
  //     "streetAddressLine1": "116-05 Myrtle Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Richmond Hill",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "11418"
  //   },
  // },
  {
    "id": "59",
    "name": "Accurate Clinical Research",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC",
    "coordinates": {
      "latitude": 29.979950,
      "longitude": -95.166460,
    },
    "address": {
      "streetAddressLine1": "17903 W Lake Houston Pkwy",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Atascosita",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "77346"
    },
  },
  // {
  //   "id": "60",
  //   "name": "Advanced Gastro P.C",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.641550,
  //     "longitude": -111.982150,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4550 E. Bell Road Bldg. 8  Suite 170",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Phoenix",
  //     "state": "AZ",
  //     "countryCode": "US",
  //     "postalCode": "85032"
  //   },
  // },
  {
    "id": "61",
    "name": "Advanced Research",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti, DUET UC",
    "coordinates": {
      "latitude": 26.2737104,
      "longitude": -80.2103146,
    },
    "address": {
      "streetAddressLine1": "6280 W. Sample Rd. Suite 202",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Coral Springs",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33067"
    },
  },
  {
    "id": "62",
    "name": "Advanced Research Center, Inc.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.82020568847656,
      "longitude": -117.90872192382812,
    },
    "address": {
      "streetAddressLine1": "1020 S. Anaheim Blvd.,",
      "streetAddressLine2": "Suite 316",
      "streetAddressLine3": null,
      "city": "Anaheim",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92805"
    },
  },
  {
    "id": "63",
    "name": "Advanced Research Institute - Ogden",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.153526306152344,
      "longitude": -111.9424057006836,
    },
    "address": {
      "streetAddressLine1": "5896 S Ridgeline Drive",
      "streetAddressLine2": "Ste. A",
      "streetAddressLine3": null,
      "city": "Ogden",
      "state": "UT",
      "countryCode": "US",
      "postalCode": "84405"
    },
  },
  // {
  //   "id": "64",
  //   "name": "Advanced Research LLC",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 26.2737104,
  //     "longitude": -80.2103146,
  //   },
  //   "address": {
  //     "streetAddressLine1": "6280 W Sample Rd.",
  //     "streetAddressLine2": "Suite 202",
  //     "streetAddressLine3": null,
  //     "city": "Coral Springs",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33067"
  //   },
  // },
  {
    "id": "65",
    "name": "AGA Clinical Research Associates, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.384699,
      "longitude": -74.570712,
    },
    "address": {
      "streetAddressLine1": "3205 Fire Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Egg Harbor Township",
      "state": "NJ",
      "countryCode": "US",
      "postalCode": "08234"
    },
  },
  {
    "id": "66",
    "name": "Albany Medical College",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.6867062,
      "longitude": -73.8135599,
    },
    "address": {
      "streetAddressLine1": "1375 Washington Ave,",
      "streetAddressLine2": "Ste 101",
      "streetAddressLine3": null,
      "city": "ALbany",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "12206"
    },
  },
  {
    "id": "67",
    "name": "Allegany Health Network",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.4569695,
      "longitude": -80.0033114,
    },
    "address": {
      "streetAddressLine1": "320 East North Avenue",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Pittsburgh",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "15224"
    },
  },
  {
    "id": "68",
    "name": "Amel Med LLC. Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.0925566,
      "longitude": -108.5970635,
    },
    "address": {
      "streetAddressLine1": "2460 Patterson Road",
      "streetAddressLine2": "Unit 4",
      "streetAddressLine3": null,
      "city": "Grand Junction",
      "state": "CO",
      "countryCode": "US",
      "postalCode": "81505"
    },
  },
  {
    "id": "69",
    "name": "Amel Med. LLC",
    "phoneNumber": null,
    "clinicType": "duet-cd",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 30.465403,
      "longitude": -97.80582,
    },
    "address": {
      "streetAddressLine1": "12701 Ranch Rd. 620 N.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Austin",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "78750"
    },
  },
  {
    "id": "70",
    "name": "Arkansas Diagnostic Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.740421,
      "longitude": -92.373161,
    },
    "address": {
      "streetAddressLine1": "8908 Kanis Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Little Rock",
      "state": "AR",
      "countryCode": "US",
      "postalCode": "72205"
    },
  },
  // {
  //   "id": "71",
  //   "name": "Ascension Medical Group Via Christi P.A.",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 37.720923,
  //     "longitude": -97.196022,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1947 N. Founders Circle",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Wichita",
  //     "state": "KS",
  //     "countryCode": "US",
  //     "postalCode": "67206"
  //   },
  // },
  {
    "id": "72",
    "name": "Atlanta Gastroenterology Associates",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.8770556,
      "longitude": -84.3577652,
    },
    "address": {
      "streetAddressLine1": "5671 Peachtree Dunwoody Rd",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Atlanta",
      "state": "GA",
      "countryCode": "US",
      "postalCode": "30342"
    },
  },
  {
    "id": "73",
    "name": "Baylor College of Medicine - Gastroeneterology",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 29.7094859,
      "longitude": -95.4030546,
    },
    "address": {
      "streetAddressLine1": "6620 Main Street",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Houston",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "77030"
    },
  },
  // {
  //   "id": "74",
  //   "name": "Brigham & Womens Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.3350287,
  //     "longitude": -71.1077129,
  //   },
  //   "address": {
  //     "streetAddressLine1": "60 Fenwood Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Boston",
  //     "state": "MA",
  //     "countryCode": "US",
  //     "postalCode": "02115"
  //   },
  // },
  // {
  //   "id": "75",
  //   "name": "Burke Internal Medicine & Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 38.765273,
  //     "longitude": -77.487121,
  //   },
  //   "address": {
  //     "streetAddressLine1": "9001 Digges Road,",
  //     "streetAddressLine2": "Suite 107",
  //     "streetAddressLine3": null,
  //     "city": "Manassas",
  //     "state": "VA",
  //     "countryCode": "US",
  //     "postalCode": "20110"
  //   },
  // },
  {
    "id": "76",
    "name": "BVL Clinical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.9215135,
      "longitude": -94.6488704,
    },
    "address": {
      "streetAddressLine1": "11401 Nall Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Leawood",
      "state": "KS",
      "countryCode": "US",
      "postalCode": "66211"
    },
  },
  // {
  //   "id": "77",
  //   "name": "BVL Clinical Research",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.2460536,
  //     "longitude": -94.4323609,
  //   },
  //   "address": {
  //     "streetAddressLine1": "840 W Kansas Street ",
  //     "streetAddressLine2": "Suite A",
  //     "streetAddressLine3": null,
  //     "city": "Liberty",
  //     "state": "MO",
  //     "countryCode": "US",
  //     "postalCode": "64068"
  //   },
  // },
  // {
  //   "id": "78",
  //   "name": "Caprock Gastro Research",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.5209731,
  //     "longitude": -101.9199445,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5115 80th Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Lubbock",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "79424"
  //   },
  // },
  // {
  //   "id": "79",
  //   "name": "Care Access",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 36.0708492,
  //     "longitude": -115.2244042,
  //   },
  //   "address": {
  //     "streetAddressLine1": "9260 West Sunset Rd,",
  //     "streetAddressLine2": "Suite 306",
  //     "streetAddressLine3": null,
  //     "city": "Las Vegas",
  //     "state": "NV",
  //     "countryCode": "US",
  //     "postalCode": "89148"
  //   },
  // },
  // {
  //   "id": "80",
  //   "name": "Care Access",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.2935776,
  //     "longitude": -77.5798113,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2541 N. Queen St. ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Kinston",
  //     "state": "NC",
  //     "countryCode": "US",
  //     "postalCode": "28501"
  //   },
  // },
  // {
  //   "id": "81",
  //   "name": "Gastroenterology Associates",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.686992,
  //     "longitude": -111.855403,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1250 East 3900 South",
  //     "streetAddressLine2": "Suite 300",
  //     "streetAddressLine3": null,
  //     "city": "Salt Lake City",
  //     "state": "UT",
  //     "countryCode": "US",
  //     "postalCode": "84124"
  //   },
  // },
  {
    "id": "82",
    "name": "Care Access Research, Scranton",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.445087,
      "longitude": -75.679392,
    },
    "address": {
      "streetAddressLine1": "201 Smallacombe Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Scranton",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "18508"
    },
  },
  {
    "id": "83",
    "name": "Carolina Digestive Health Associates",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.1568787,
      "longitude": -80.8483624,
    },
    "address": {
      "streetAddressLine1": "10620 Park Road",
      "streetAddressLine2": "Suite 102",
      "streetAddressLine3": null,
      "city": "Charlotte",
      "state": "NC",
      "countryCode": "US",
      "postalCode": "28210"
    },
  },
  // {
  //   "id": "84",
  //   "name": "Carolina Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.501272,
  //     "longitude": -77.986463,
  //   },
  //   "address": {
  //     "streetAddressLine1": "704 WH Smith Blvd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "NC",
  //     "state": "Greenville",
  //     "countryCode": "US",
  //     "postalCode": "27834"
  //   },
  // },
  // {
  //   "id": "85",
  //   "name": "Carolinas Healthcare System",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.2087414,
  //     "longitude": -80.8224877,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2015 Randolph",
  //     "streetAddressLine2": "Suite 208",
  //     "streetAddressLine3": "RoadCharlotte Gastroenterology & Hepatology",
  //     "city": "Charlotte",
  //     "state": "NC",
  //     "countryCode": "US",
  //     "postalCode": "28207"
  //   },
  // },
  {
    "id": "86",
    "name": "Cedars Sinai Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.0749537,
      "longitude": -118.3821777,
    },
    "address": {
      "streetAddressLine1": "8730 Alden Drive,",
      "streetAddressLine2": "Thalians W220",
      "streetAddressLine3": null,
      "city": "Los Angeles",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "90048"
    },
  },
  {
    "id": "87",
    "name": "Center for Advanced Gastroenterology",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 28.630211,
      "longitude": -81.376262,
    },
    "address": {
      "streetAddressLine1": "CFAGI LLC 740 Concourse Parkway South",
      "streetAddressLine2": "Suite 200",
      "streetAddressLine3": null,
      "city": "Maitland",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "32751"
    },
  },
  // {
  //   "id": "88",
  //   "name": "Center for Digestive Health",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.567914,
  //     "longitude": -81.364378,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1817 North Mills Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Orlando",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "32803"
  //   },
  // },
  // {
  //   "id": "89",
  //   "name": "Central Arkansas Veterans Administration Hospital (CAVHS)",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.744955,
  //     "longitude": -92.321036,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4300 West 7th Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Little Rock",
  //     "state": "AK",
  //     "countryCode": "US",
  //     "postalCode": "72205"
  //   },
  // },
  // {
  //   "id": "90",
  //   "name": "Central Florida Gastro Research, LLC",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.300882,
  //     "longitude": -81.400368,
  //   },
  //   "address": {
  //     "streetAddressLine1": "901 East Oak Street",
  //     "streetAddressLine2": "Suite C",
  //     "streetAddressLine3": null,
  //     "city": "Kissimmee",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "34744"
  //   },
  // },
  // {
  //   "id": "91",
  //   "name": "Central Ohio Endoscopy Center LLC",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.0151338,
  //     "longitude": -83.0225122,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3773 Olentangy River Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Columbus",
  //     "state": "OH",
  //     "countryCode": "US",
  //     "postalCode": "43214"
  //   },
  // },
  // {
  //   "id": "92",
  //   "name": "Central Sooner Research",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.229754,
  //     "longitude": -97.44119,
  //   },
  //   "address": {
  //     "streetAddressLine1": "900 North Porter",
  //     "streetAddressLine2": "Suite 207",
  //     "streetAddressLine3": null,
  //     "city": "Norman",
  //     "state": "OK",
  //     "countryCode": "US",
  //     "postalCode": "73071"
  //   },
  // },
  // {
  //   "id": "93",
  //   "name": "Centre Hospitalier de l Universite de Montreal (CHUM)",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.512228,
  //     "longitude": -73.557677,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1000 St Denis street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Montreal",
  //     "state": "Quebec",
  //     "countryCode": "CA",
  //     "postalCode": "H2X 3J4"
  //   },
  // },
  // {
  //   "id": "94",
  //   "name": "Circuit Clinical",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.4486946,
  //     "longitude": -74.3625679,
  //   },
  //   "address": {
  //     "streetAddressLine1": "75 Crystal Run Rd",
  //     "streetAddressLine2": "Suite 104",
  //     "streetAddressLine3": null,
  //     "city": "Middletown",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10940"
  //   },
  // },
  // {
  //   "id": "95",
  //   "name": "CISSS de Laurentides Hospital Regional de Saint Jerome",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.767118,
  //     "longitude": -73.999746,
  //   },
  //   "address": {
  //     "streetAddressLine1": "290 Montigny,",
  //     "streetAddressLine2": "Block F",
  //     "streetAddressLine3": null,
  //     "city": "Saint Jerome",
  //     "state": "Quebec",
  //     "countryCode": "CA",
  //     "postalCode": "J7Z 5T3"
  //   },
  // },
  // {
  //   "id": "96",
  //   "name": "CIUSSS Saguenay Lac St Jean",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 48.424912,
  //     "longitude": -71.047851,
  //   },
  //   "address": {
  //     "streetAddressLine1": "305 St Vallier,",
  //     "streetAddressLine2": "Pavillon des Augustines,",
  //     "streetAddressLine3": " 3e etage CP 221",
  //     "city": "Chicoutimi",
  //     "state": "Quebec",
  //     "countryCode": "CA",
  //     "postalCode": "G7H 7M4"
  //   },
  // },
  {
    "id": "97",
    "name": "Clearview Medical Research, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.4316036,
      "longitude": -118.5338403,
    },
    "address": {
      "streetAddressLine1": "26781 Bouquet Canyon Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Santa Clarita",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "91350"
    },
  },
  // {
  //   "id": "98",
  //   "name": "Cleveland Clinic Foundation",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.5021719,
  //     "longitude": -81.6198195,
  //   },
  //   "address": {
  //     "streetAddressLine1": "9500 Euclid Ave.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Cleveland",
  //     "state": "OH",
  //     "countryCode": "US",
  //     "postalCode": "44195"
  //   },
  // },
  // {
  //   "id": "99",
  //   "name": "Clinical Applications Laboratories, Inc",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.7401167,
  //     "longitude": -117.1622753,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3330 3rd Ave",
  //     "streetAddressLine2": "Suite 303",
  //     "streetAddressLine3": null,
  //     "city": "San Diego",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92103"
  //   },
  // },
  {
    "id": "100",
    "name": "Clinical Research Enterprise",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.503235,
      "longitude": -86.7981873,
    },
    "address": {
      "streetAddressLine1": "930 20th Street South,",
      "streetAddressLine2": "CH20 383",
      "streetAddressLine3": null,
      "city": "Birmingham",
      "state": "AL",
      "countryCode": "US",
      "postalCode": "35205"
    },
  },
  {
    "id": "101",
    "name": "Clinical Research Institute of Michigan, LLC",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.6752437,
      "longitude": -82.786494,
    },
    "address": {
      "streetAddressLine1": "30795 23 Mile Rd.",
      "streetAddressLine2": "Suite 206",
      "streetAddressLine3": null,
      "city": "Chesterfield Township",
      "state": "MI",
      "countryCode": "US",
      "postalCode": "48047"
    },
  },
  // {
  //   "id": "102",
  //   "name": "Clinical Research of California",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 37.9049152,
  //     "longitude": -122.0690274,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1399 Ygnacio Valley Rd,",
  //     "streetAddressLine2": " #11c",
  //     "streetAddressLine3": null,
  //     "city": "Walnut Creek",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "94598"
  //   },
  // },
  // {
  //   "id": "103",
  //   "name": "Clinnova Research Solutions",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.780696,
  //     "longitude": -117.8598585,
  //   },
  //   "address": {
  //     "streetAddressLine1": "705 W. La Veta Ave.",
  //     "streetAddressLine2": "Suite 109",
  //     "streetAddressLine3": null,
  //     "city": "Orange",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92868"
  //   },
  // },
  {
    "id": "104",
    "name": "Columbia University Hospital - Herbert Irving Pavilion",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.8406015,
      "longitude": -73.9431492,
    },
    "address": {
      "streetAddressLine1": "161 Fort Washington Ave,",
      "streetAddressLine2": "8th floor",
      "streetAddressLine3": "(Dept of Colorectal Surgery)",
      "city": "New York",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "10032"
    },
  },
  // {
  //   "id": "105",
  //   "name": "Columbus Clinical Services",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 25.7741795,
  //     "longitude": -80.2394249,
  //   },
  //   "address": {
  //     "streetAddressLine1": "110 Northwest 27th Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Miami",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33125"
  //   },
  // },
  {
    "id": "106",
    "name": "Combined Gastro Research, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 30.202432,
      "longitude": -92.018291,
    },
    "address": {
      "streetAddressLine1": "1211 Coolidge Blvd, Suite 400",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Lafayette",
      "state": "LA",
      "countryCode": "US",
      "postalCode": "70503"
    },
  },
  // {
  //   "id": "107",
  //   "name": "Cotton O'Neil Clinical Research Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.0542696,
  //     "longitude": -95.6923122,
  //   },
  //   "address": {
  //     "streetAddressLine1": "720 SW Lane Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Topeka",
  //     "state": "KS",
  //     "countryCode": "US",
  //     "postalCode": "66606"
  //   },
  // },
  {
    "id": "108",
    "name": "Crystal Run Healthcare LLP",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.4523333,
      "longitude": -74.3570411,
    },
    "address": {
      "streetAddressLine1": "155 Crystal Run Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Middletown",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "10941"
    },
  },
  // {
  //   "id": "109",
  //   "name": "Dayton Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.763861,
  //     "longitude": -84.192905,
  //   },
  //   "address": {
  //     "streetAddressLine1": "9000 North Main St.",
  //     "streetAddressLine2": "Suite 405",
  //     "streetAddressLine3": null,
  //     "city": "Dayton",
  //     "state": "OH",
  //     "countryCode": "US",
  //     "postalCode": "45415"
  //   },
  // },
  {
    "id": "110",
    "name": "Del Sol Research Management, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.243685,
      "longitude": -110.8569267,
    },
    "address": {
      "streetAddressLine1": "6369 E. Tanque Verde Rd  Suite 200  ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Tucson",
      "state": "AZ",
      "countryCode": "US",
      "postalCode": "85715"
    },
  },
  // {
  //   "id": "111",
  //   "name": "Detroit Clinical Research Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.243685,
  //     "longitude": -110.8569267,
  //   },
  //   "address": {
  //     "streetAddressLine1": "30160 Orchard Lake Rd,",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Farmington Hills",
  //     "state": "MI",
  //     "countryCode": "US",
  //     "postalCode": "85715"
  //   },
  // },
  {
    "id": "112",
    "name": "Digestive and Liver Disease Institute",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 28.3009915,
      "longitude": -81.4089425,
    },
    "address": {
      "streetAddressLine1": "505 W Oak St,",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Kissimmee",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34741"
    },
  },
  {
    "id": "113",
    "name": "Digestive Disease Associates",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC",
    "coordinates": {
      "latitude": 34.9535068,
      "longitude": -81.0481469,
    },
    "address": {
      "streetAddressLine1": "170 Amendment Avenue",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Rock Hill",
      "state": "SC",
      "countryCode": "US",
      "postalCode": "29732"
    },
  },
  {
    "id": "114",
    "name": "Digestive Disease Medicine of Central New York, LLP",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 43.0903347,
      "longitude": -75.2849224,
    },
    "address": {
      "streetAddressLine1": "116 Business Park Drive,",
      "streetAddressLine2": "First Floor",
      "streetAddressLine3": null,
      "city": "Utica",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "13502"
    },
  },
  // {
  //   "id": "115",
  //   "name": "Digestive Health Center",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.8176123,
  //     "longitude": -96.8410405,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2201 Inwood Rd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Dallas",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "75235"
  //   },
  // },
  // {
  //   "id": "116",
  //   "name": "Digestive Health of the SE",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 31.2230389,
  //     "longitude": -85.4348376,
  //   },
  //   "address": {
  //     "streetAddressLine1": "480 Honeysuckle Rd.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Dothan",
  //     "state": "AL",
  //     "countryCode": "US",
  //     "postalCode": "36305"
  //   },
  // },
  {
    "id": "117",
    "name": "Digestive Health Specialists",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.6027817,
      "longitude": -71.350969,
    },
    "address": {
      "streetAddressLine1": "4 Meeting House Rd,",
      "streetAddressLine2": "Suite 6-8",
      "streetAddressLine3": null,
      "city": "Chelmsford",
      "state": "MA",
      "countryCode": "US",
      "postalCode": "01824"
    },
  },
  // {
  //   "id": "118",
  //   "name": "Tyler Research Institute, LLC",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.3310178,
  //     "longitude": -95.2905353,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1720 South Beckham Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Tyler",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "75701"
  //   },
  // },
  {
    "id": "119",
    "name": "Digestive Health Specialists PC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.6027817,
      "longitude": -71.350969,
    },
    "address": {
      "streetAddressLine1": "4 Meeting House Rd",
      "streetAddressLine2": "Suite 6",
      "streetAddressLine3": null,
      "city": "Chelmsford",
      "state": "MA",
      "countryCode": "US",
      "postalCode": "01824"
    },
  },
  // {
  //   "id": "120",
  //   "name": "DiGiovanna Institute for Med Education\Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.7106533,
  //     "longitude": -73.4613572,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1061 North Broadway  2nd floor  ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "North Massapequa",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "11758"
  //   },
  // },
  {
    "id": "121",
    "name": "East Tennessee Research Institute",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 36.3058848,
      "longitude": -82.386204,
    },
    "address": {
      "streetAddressLine1": "310 N State of Franklin Road, Suite 202",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Johnson City",
      "state": "TN",
      "countryCode": "US",
      "postalCode": "37604"
    },
  },
  // {
  //   "id": "122",
  //   "name": "Essential Medical Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 36.0319546,
  //     "longitude": -95.9238075,
  //   },
  //   "address": {
  //     "streetAddressLine1": "4765 East 91st Street Suite 200    ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Tulsa",
  //     "state": "OK",
  //     "countryCode": "US",
  //     "postalCode": "74137"
  //   },
  // },
  {
    "id": "123",
    "name": "FC Research, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.6723828,
      "longitude": -70.9877531,
    },
    "address": {
      "streetAddressLine1": "535 Faunce Corner Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Dartmouth",
      "state": "MA",
      "countryCode": "US",
      "postalCode": "02747"
    },
  },
  {
    "id": "124",
    "name": "Florida Research Institute",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.462289,
      "longitude": -82.438426,
    },
    "address": {
      "streetAddressLine1": "10910 Technology Terrace",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Lakewood Ranch",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34211"
    },
  },
  // {
  //   "id": "125",
  //   "name": "Foothills Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 51.065425,
  //     "longitude": -114.131745,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1403 29 St NW",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Calgary",
  //     "state": "AB",
  //     "countryCode": "CA",
  //     "postalCode": "T2N 4J8"
  //   },
  // },
  // {
  //   "id": "126",
  //   "name": "Fraser Clinical Trials, Inc.",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.22509,
  //     "longitude": -122.893362,
  //   },
  //   "address": {
  //     "streetAddressLine1": "106-245 Columbia Street East",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "New Westminster",
  //     "state": "BC",
  //     "countryCode": "US",
  //     "postalCode": "V3L 3W4"
  //   },
  // },
  {
    "id": "127",
    "name": "Frontier Clinical Research LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.899659,
      "longitude": -79.755759,
    },
    "address": {
      "streetAddressLine1": "300 Spring Creek Ln    ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Uniontown",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "15401"
    },
  },
  {
    "id": "128",
    "name": "Gastro Florida",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.966001,
      "longitude": -82.708885,
    },
    "address": {
      "streetAddressLine1": "2454 McMullen Booth Rd",
      "streetAddressLine2": "Suite 609 Suite 610",
      "streetAddressLine3": null,
      "city": "Clearwater",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33759"
    },
  },
  // {
  //   "id": "129",
  //   "name": "GASTRO HEALTH & NUTRITION",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.8514077,
  //     "longitude": -97.015741,
  //   },
  //   "address": {
  //     "streetAddressLine1": "106 Springwood Drive",
  //     "streetAddressLine2": "Suite 100",
  //     "streetAddressLine3": null,
  //     "city": "Victoria",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "77904"
  //   },
  // },
  {
    "id": "130",
    "name": "Gastro One",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.1135371,
      "longitude": -89.8073868,
    },
    "address": {
      "streetAddressLine1": "1324 Wolf Park Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Germantown",
      "state": "TN",
      "countryCode": "US",
      "postalCode": "38138"
    },
  },
  // {
  //   "id": "131",
  //   "name": "Gastroent. Assoc. of Cent. GA",
  //   "phoneNumber": null,
  //   "clinicType": "multiple",
  //   "clinicTypes": "DUET CD, Graviti",
  //   "coordinates": {
  //     "latitude": 32.832827,
  //     "longitude": -83.629782,
  //   },
  //   "address": {
  //     "streetAddressLine1": "610 Third Street, Suite 204    ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Macon",
  //     "state": "GA",
  //     "countryCode": "US",
  //     "postalCode": "31201"
  //   },
  // },
  {
    "id": "132",
    "name": "Gastroenterolgy Assoc of Orangeburg",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.5377951,
      "longitude": -80.8252951,
    },
    "address": {
      "streetAddressLine1": "1131 Cook Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Orangeburg",
      "state": "SC",
      "countryCode": "US",
      "postalCode": "29118"
    },
  },
  // {
  //   "id": "133",
  //   "name": "Gastroenterolgy Group of Naples",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 26.161015,
  //     "longitude": -81.791012,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1064 Goodlette Rd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Naples",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "34102"
  //   },
  // },
  // {
  //   "id": "134",
  //   "name": "Gastroenterology and Internal Medicine Research Institute",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 53.520468,
  //     "longitude": -113.588944,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8708 155 Street",
  //     "streetAddressLine2": "Suite 408",
  //     "streetAddressLine3": null,
  //     "city": "Edmonton",
  //     "state": "AB",
  //     "countryCode": "US",
  //     "postalCode": "T5R 1W2"
  //   },
  // },
  // {
  //   "id": "135",
  //   "name": "Gastroenterology Associates of Central GA",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.832827,
  //     "longitude": -83.629782,
  //   },
  //   "address": {
  //     "streetAddressLine1": "610 Third Street Suite 204",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Macon",
  //     "state": "GA",
  //     "countryCode": "US",
  //     "postalCode": "31201"
  //   },
  // },
  // {
  //   "id": "136",
  //   "name": "Gastroenterology Associates of Central Georgia",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.832827,
  //     "longitude": -83.629782,
  //   },
  //   "address": {
  //     "streetAddressLine1": "610 Third Street,",
  //     "streetAddressLine2": "Ste. 204",
  //     "streetAddressLine3": null,
  //     "city": "Macon",
  //     "state": "GA",
  //     "countryCode": "US",
  //     "postalCode": "31201"
  //   },
  // },
  {
    "id": "137",
    "name": "Gastroenterology Associates of Tidewater",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 36.7417502,
      "longitude": -76.2461723,
    },
    "address": {
      "streetAddressLine1": "112 Gainsborough Square",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Chesapeake",
      "state": "VA",
      "countryCode": "US",
      "postalCode": "23320"
    },
  },
  // {
  //   "id": "138",
  //   "name": "Gastroenterology Associates P.A.",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.7191628,
  //     "longitude": -82.247647,
  //   },
  //   "address": {
  //     "streetAddressLine1": "112B Hospital Dr",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Simpsonville",
  //     "state": "SC",
  //     "countryCode": "US",
  //     "postalCode": "29681"
  //   },
  // },
  {
    "id": "139",
    "name": "Gastroenterology Consultants, P.C.",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.0647243,
      "longitude": -84.3213435,
    },
    "address": {
      "streetAddressLine1": "11685 Alpharetta Highway",
      "streetAddressLine2": "  Suite 420/325",
      "streetAddressLine3": null,
      "city": "Roswell",
      "state": "GA",
      "countryCode": "US",
      "postalCode": "30076"
    },
  },
  {
    "id": "140",
    "name": "Gastroenterology Group of Naples",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti, DUET UC",
    "coordinates": {
      "latitude": 26.161173,
      "longitude": -81.791015,
    },
    "address": {
      "streetAddressLine1": "1080 Goodlette Road",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Naples",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34102"
    },
  },
  // {
  //   "id": "141",
  //   "name": "Gastroenterology Health Partners, PLLC",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 38.3147474,
  //     "longitude": -85.8207755,
  //   },
  //   "address": {
  //     "streetAddressLine1": "2630 Grant Line Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "New Albany",
  //     "state": "IN",
  //     "countryCode": "US",
  //     "postalCode": "47150"
  //   },
  // },
  {
    "id": "142",
    "name": "Gastrointestinal Assoc. of NE TN",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 36.3058848,
      "longitude": -82.386204,
    },
    "address": {
      "streetAddressLine1": "310 N State of Franklin Rd.",
      "streetAddressLine2": "Suite 102, 201, 202 and 204",
      "streetAddressLine3": null,
      "city": "Johnson City",
      "state": "TN",
      "countryCode": "US",
      "postalCode": "37604"
    },
  },
  {
    "id": "143",
    "name": "Gastrointestinal Specialists of Georgia",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.9703142,
      "longitude": -84.5477616,
    },
    "address": {
      "streetAddressLine1": "711 Canton Road,",
      "streetAddressLine2": "Suite 300",
      "streetAddressLine3": null,
      "city": "Marietta",
      "state": "GA",
      "countryCode": "US",
      "postalCode": "30060"
    },
  },
  // {
  //   "id": "144",
  //   "name": "GI Alliance",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 47.6212017,
  //     "longitude": -122.1857771,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1135 116th Ave NE,",
  //     "streetAddressLine2": " # LL-160",
  //     "streetAddressLine3": null,
  //     "city": "Bellevue",
  //     "state": "WA",
  //     "countryCode": "US",
  //     "postalCode": "98004"
  //   },
  // },
  {
    "id": "145",
    "name": "GI Alliance",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.3395409,
      "longitude": -90.0639517,
    },
    "address": {
      "streetAddressLine1": "2510 Lakeland Dr.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Flowood",
      "state": "MS",
      "countryCode": "US",
      "postalCode": "39232"
    },
  },
  // {
  //   "id": "146",
  //   "name": "GI Research Institute",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.278621,
  //     "longitude": -123.12696,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1190 Hornby Street",
  //     "streetAddressLine2": "Unit 770",
  //     "streetAddressLine3": null,
  //     "city": "Vancouver",
  //     "state": "BC",
  //     "countryCode": "US",
  //     "postalCode": "V6Z 2K5"
  //   },
  // },
  {
    "id": "147",
    "name": "Grand Teton Research Group, PLLC",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 43.471768,
      "longitude": -111.988734,
    },
    "address": {
      "streetAddressLine1": "2770 Cortez Ave.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Idaho Falls",
      "state": "ID",
      "countryCode": "US",
      "postalCode": "83404"
    },
  },
  // {
  //   "id": "148",
  //   "name": "Great Lakes Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.6727487,
  //     "longitude": -81.3264944,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8877 Mentor Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Mentor",
  //     "state": "OH",
  //     "countryCode": "US",
  //     "postalCode": "44060"
  //   },
  // },
  // {
  //   "id": "149",
  //   "name": "Harmony Medical Research Institute, Inc",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 25.8955429,
  //     "longitude": -80.3357976,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8001 West 26th Avenue",
  //     "streetAddressLine2": "Unit 3",
  //     "streetAddressLine3": null,
  //     "city": "Hialeah",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33016"
  //   },
  // },
  // {
  //   "id": "150",
  //   "name": "Health Sciences Centre Winnipeg",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.903595,
  //     "longitude": -97.157159,
  //   },
  //   "address": {
  //     "streetAddressLine1": "MS783-820 Sherbrook St",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Winnipeg",
  //     "state": "MB",
  //     "countryCode": "CA",
  //     "postalCode": "R3A 1R9"
  //   },
  // },
  // {
  //   "id": "151",
  //   "name": "Health Sciences Centre-Winnipeg Regional Health Authority",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.903595,
  //     "longitude": -97.157159,
  //   },
  //   "address": {
  //     "streetAddressLine1": "MS783-820 Sherbrook Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Winnipeg",
  //     "state": "MB",
  //     "countryCode": "US",
  //     "postalCode": "R3A 1R9"
  //   },
  // },
  {
    "id": "152",
    "name": "Hoag Memorial Hospital Presbyterian",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.625736,
      "longitude": -117.930663,
    },
    "address": {
      "streetAddressLine1": "One Hoag Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Newport Beach",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92658"
    },
  },
  {
    "id": "153",
    "name": "Homestead Associates in Research Inc.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 25.5107032,
      "longitude": -80.4346811,
    },
    "address": {
      "streetAddressLine1": "27535 S Dixie Hwy,",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Miami,",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33189"
    },
  },
  // {
  //   "id": "154",
  //   "name": "Hospital Maisonneuve-Rosemont",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.5731196,
  //     "longitude": -73.5576414,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5345 Assomption Blvd,",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Montreal",
  //     "state": "QC",
  //     "countryCode": "CA",
  //     "postalCode": "H1T 4B3"
  //   },
  // },
  {
    "id": "155",
    "name": "Hunter Holmes McGuire VAMC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 37.4963453,
      "longitude": -77.4656428,
    },
    "address": {
      "streetAddressLine1": "1201 Broad Rock Blvd",
      "streetAddressLine2": "111 N",
      "streetAddressLine3": null,
      "city": "Richmond",
      "state": "VA",
      "countryCode": "US",
      "postalCode": "23249"
    },
  },
  // {
  //   "id": "156",
  //   "name": "Huron Gastro",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 42.26612,
  //     "longitude": -83.658681,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5300 Elliott Drive",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Ypsilanti",
  //     "state": "MI",
  //     "countryCode": "US",
  //     "postalCode": "48197"
  //   },
  // },
  {
    "id": "157",
    "name": "Huron Gastroenterology Associates Center for Digestive Care",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.26612,
      "longitude": -83.658681,
    },
    "address": {
      "streetAddressLine1": "5300 Elliott Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Ypsilanti",
      "state": "MI",
      "countryCode": "US",
      "postalCode": "48197"
    },
  },
  // {
  //   "id": "158",
  //   "name": "IHS Health Research",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.3009941,
  //     "longitude": -81.4084011,
  //   },
  //   "address": {
  //     "streetAddressLine1": "445 West Oak Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Kissimmee",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "34741"
  //   },
  // },
  {
    "id": "159",
    "name": "Indiana University Health, University Hospital",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.7759498,
      "longitude": -86.1766939,
    },
    "address": {
      "streetAddressLine1": "550 N University Boulevard.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Indianapolis",
      "state": "IN",
      "countryCode": "US",
      "postalCode": "46202"
    },
  },
  {
    "id": "160",
    "name": "InSite Digestive Health Care",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC",
    "coordinates": {
      "latitude": 34.1424632,
      "longitude": -118.0208191,
    },
    "address": {
      "streetAddressLine1": "488 E. Santa Clara Street,",
      "streetAddressLine2": "Suite 103",
      "streetAddressLine3": null,
      "city": "Arcadia",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "91006"
    },
  },
  // {
  //   "id": "161",
  //   "name": "Installation Hopital Maisonneuve-Rosemont",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.5729738,
  //     "longitude": -73.5581639,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5415 boul. l Assomption; Pavillon Rachel-Tourigny,",
  //     "streetAddressLine2": "porte 4164",
  //     "streetAddressLine3": null,
  //     "city": "Montreal",
  //     "state": "QB",
  //     "countryCode": "CA",
  //     "postalCode": "H1T 2M4"
  //   },
  // },
  // {
  //   "id": "162",
  //   "name": "Institute for Digestive Health and Liver Disease",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.2934211,
  //     "longitude": -76.6129185,
  //   },
  //   "address": {
  //     "streetAddressLine1": "301 Saint Paul Place",
  //     "streetAddressLine2": "Medical Staff Office",
  //     "streetAddressLine3": null,
  //     "city": "Baltimore",
  //     "state": "MD",
  //     "countryCode": "US",
  //     "postalCode": "21202"
  //   },
  // },
  // {
  //   "id": "163",
  //   "name": "Integris Baptist Office",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.511007,
  //     "longitude": -97.578865,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3366 NW Expressway",
  //     "streetAddressLine2": "Building D  Suite 400",
  //     "streetAddressLine3": null,
  //     "city": "Oklahoma City",
  //     "state": "OK",
  //     "countryCode": "US",
  //     "postalCode": "73112"
  //   },
  // },
  {
    "id": "164",
    "name": "IU Health University Hospital",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.7759498,
      "longitude": -86.1766939,
    },
    "address": {
      "streetAddressLine1": "550 North University Blvd    ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Indianapolis",
      "state": "IN",
      "countryCode": "US",
      "postalCode": "46202"
    },
  },
  {
    "id": "165",
    "name": "Javara",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.1500577,
      "longitude": -80.8400495,
    },
    "address": {
      "streetAddressLine1": "6060 Piedmonth Row Drive South,",
      "streetAddressLine2": "3rd Floor",
      "streetAddressLine3": null,
      "city": "Charlotte",
      "state": "NC",
      "countryCode": "US",
      "postalCode": "28287"
    },
  },
  // {
  //   "id": "166",
  //   "name": "Lemah Creek Clinical Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 41.6686077,
  //     "longitude": -88.1254026,
  //   },
  //   "address": {
  //     "streetAddressLine1": "319 North Weber Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Bolingbrook",
  //     "state": "IL",
  //     "countryCode": "US",
  //     "postalCode": "60490"
  //   },
  // },
  {
    "id": "167",
    "name": "Lenox Hill Hospital",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.7736428,
      "longitude": -73.9608619,
    },
    "address": {
      "streetAddressLine1": "100 E 77th St",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "New York",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "10075"
    },
  },
  // {
  //   "id": "168",
  //   "name": "London Health Sciences Centre University Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.010969,
  //     "longitude": -81.276901,
  //   },
  //   "address": {
  //     "streetAddressLine1": "339 Windermere Rd.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "London",
  //     "state": "ON",
  //     "countryCode": "CA",
  //     "postalCode": "N6A 5A5"
  //   },
  // },
  // {
  //   "id": "169",
  //   "name": "London Health Sciences Centre University Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.010969,
  //     "longitude": -81.276901,
  //   },
  //   "address": {
  //     "streetAddressLine1": "339 Windermere Road University Hospital ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "London",
  //     "state": "ON",
  //     "countryCode": "CA",
  //     "postalCode": "N6A 5A5"
  //   },
  // },
  {
    "id": "170",
    "name": "Marshall Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.406915,
      "longitude": -82.426197,
    },
    "address": {
      "streetAddressLine1": "1600 Medical Center Drive",
      "streetAddressLine2": "Suite 250",
      "streetAddressLine3": null,
      "city": "Huntington",
      "state": "WV",
      "countryCode": "US",
      "postalCode": "25701"
    },
  },
  {
    "id": "171",
    "name": "Mayo Clinic",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 44.0222914,
      "longitude": -92.4676142,
    },
    "address": {
      "streetAddressLine1": "200 1st Street Southwest  ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Rochester",
      "state": "MN",
      "countryCode": "US",
      "postalCode": "55905"
    },
  },
  // {
  //   "id": "172",
  //   "name": "Medical Arts Health Research Group",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.8792706,
  //     "longitude": -119.4580782,
  //   },
  //   "address": {
  //     "streetAddressLine1": "360-1855 Kirschner Road",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Kelowna",
  //     "state": "BC",
  //     "countryCode": "CA",
  //     "postalCode": "J4Y 0K7"
  //   },
  // },
  {
    "id": "173",
    "name": "Medical Associates Research group",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.800852,
      "longitude": -117.152558,
    },
    "address": {
      "streetAddressLine1": "8008 Frost St,",
      "streetAddressLine2": "Ste 200",
      "streetAddressLine3": null,
      "city": "San Diego",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92123"
    },
  },
  {
    "id": "174",
    "name": "Medical Research Center of Connecticut",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.3767766,
      "longitude": -72.9048342,
    },
    "address": {
      "streetAddressLine1": "2200 Whitney Avenue,",
      "streetAddressLine2": "Suite 370",
      "streetAddressLine3": null,
      "city": "Hamden",
      "state": "CT",
      "countryCode": "US",
      "postalCode": "06518"
    },
  },
  {
    "id": "175",
    "name": "Medical University of South Carolina",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.782624,
      "longitude": -79.950597,
    },
    "address": {
      "streetAddressLine1": "25 Courtenay Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Charleston",
      "state": "SC",
      "countryCode": "US",
      "postalCode": "29425"
    },
  },
  {
    "id": "176",
    "name": "MedStar Georgetown University Hospital",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.9116477,
      "longitude": -77.0752976,
    },
    "address": {
      "streetAddressLine1": "3800 Reservoir Road NW",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Washington DC",
      "state": 'DC',
      "countryCode": "US",
      "postalCode": "20007"
    },
  },
  {
    "id": "177",
    "name": "MGG Group Co, Inc.Chevy Chase Clinical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.964019,
      "longitude": -77.089481,
    },
    "address": {
      "streetAddressLine1": "5550 Friendship Boulevard,",
      "streetAddressLine2": "Suite T-90",
      "streetAddressLine3": null,
      "city": "Chevy Chase",
      "state": "MD",
      "countryCode": "US",
      "postalCode": "20815"
    },
  },
  // {
  //   "id": "178",
  //   "name": "Mount Sinai Hospital-Toronto-Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.6564008,
  //     "longitude": -79.3891461,
  //   },
  //   "address": {
  //     "streetAddressLine1": "445-600 University Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Toronto",
  //     "state": "ON",
  //     "countryCode": "CA",
  //     "postalCode": "M5G 1X5"
  //   },
  // },
  // {
  //   "id": "179",
  //   "name": "New York Gastroenterology Associates",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.772941,
  //     "longitude": -73.953892,
  //   },
  //   "address": {
  //     "streetAddressLine1": "311 East 79th Street",
  //     "streetAddressLine2": "Suite 2A",
  //     "streetAddressLine3": null,
  //     "city": "New York",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "10075"
  //   },
  // },
  // {
  //   "id": "180",
  //   "name": "North Bay Regional Health Centre McKercher",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 46.315567,
  //     "longitude": -79.43993,
  //   },
  //   "address": {
  //     "streetAddressLine1": "Northgate Square,",
  //     "streetAddressLine2": "Suite 208",
  //     "streetAddressLine3": "1500 Fisher Street",
  //     "city": "North Bay",
  //     "state": "ON",
  //     "countryCode": "US",
  //     "postalCode": "P1B 2H3"
  //   },
  // },
  {
    "id": "181",
    "name": "Northshore Gastroenterology Research",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "ASTRO, DUET CD",
    "coordinates": {
      "latitude": 41.4750465,
      "longitude": -81.9030388,
    },
    "address": {
      "streetAddressLine1": "850 Columbia Road",
      "streetAddressLine2": "Suite 200",
      "streetAddressLine3": null,
      "city": "Westlake",
      "state": "OH",
      "countryCode": "US",
      "postalCode": "44145"
    },
  },
  // {
  //   "id": "182",
  //   "name": "Nova Scotia Health Authority - Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 44.6383547,
  //     "longitude": -63.5821305,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5790 Univ Ave.",
  //     "streetAddressLine2": "Rm 321D",
  //     "streetAddressLine3": null,
  //     "city": "Halifax",
  //     "state": "NS",
  //     "countryCode": "CA",
  //     "postalCode": "B3H 1V7"
  //   },
  // },
  // {
  //   "id": "183",
  //   "name": "Nova Scotia Health Authority-Queen Elizabeth II Health Sciences Centre-Victoria General Site",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 44.6379204,
  //     "longitude": -63.5800467,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1276 Sout Park Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Halifax",
  //     "state": "NS",
  //     "countryCode": "CA",
  //     "postalCode": "B3H 2Y9"
  //   },
  // },
  {
    "id": "184",
    "name": "Nuvance Health Medical Practice CT, Inc. Gastroenterology",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.4116049,
      "longitude": -73.4316556,
    },
    "address": {
      "streetAddressLine1": "69 Sand Pit Road",
      "streetAddressLine2": "Suite 300",
      "streetAddressLine3": null,
      "city": "Danbury",
      "state": "CT",
      "countryCode": "US",
      "postalCode": "06810"
    },
  },
  // {
  //   "id": "185",
  //   "name": "Ochsner Medical Center-Jefferson Highway",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 29.9610677,
  //     "longitude": -90.1456737,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1514 Jefferson Highway,",
  //     "streetAddressLine2": "Gastroenterology Dept.",
  //     "streetAddressLine3": " 4th Floor Rm 4D4324",
  //     "city": "New Orleans",
  //     "state": "LA",
  //     "countryCode": "US",
  //     "postalCode": "70121"
  //   },
  // },
  {
    "id": "186",
    "name": "OM Research LLC",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti, DUET UC",
    "coordinates": {
      "latitude": 34.5439445,
      "longitude": -117.2697163,
    },
    "address": {
      "streetAddressLine1": "16018 Tuscola Rd.,",
      "streetAddressLine2": "Suite 2",
      "streetAddressLine3": null,
      "city": "Apple Valley",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92307"
    },
  },
  // {
  //   "id": "187",
  //   "name": "Omega Research Orlando",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 28.873075,
  //     "longitude": -81.286383,
  //   },
  //   "address": {
  //     "streetAddressLine1": "609 N. Charles Richard Beall Blvd.",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Debary",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "32713"
  //   },
  // },
  {
    "id": "188",
    "name": "Omega Research Orlando, LLC",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC, Graviti",
    "coordinates": {
      "latitude": 28.55352,
      "longitude": -81.4305104,
    },
    "address": {
      "streetAddressLine1": "816 Mercy Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Orlando",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "32808"
    },
  },
  {
    "id": "189",
    "name": "OnSite Clinical Solutions, LLC",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.681523,
      "longitude": -80.471561,
    },
    "address": {
      "streetAddressLine1": "611 Mocksville Ave",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Salisbury",
      "state": "NC",
      "countryCode": "US",
      "postalCode": "28144"
    },
  },
  {
    "id": "190",
    "name": "Penn Health -Princeton",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.332986,
      "longitude": -74.60737,
    },
    "address": {
      "streetAddressLine1": "5 Plainsboro Road",
      "streetAddressLine2": "450",
      "streetAddressLine3": null,
      "city": "Plainsboro",
      "state": "NJ",
      "countryCode": "US",
      "postalCode": "08536"
    },
  },
  // {
  //   "id": "191",
  //   "name": "PerCuro Clinical Research Ltd.",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 48.461342,
  //     "longitude": -123.332374,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1590 Cedar Hill Cross Rd ",
  //     "streetAddressLine2": "Suite 2320",
  //     "streetAddressLine3": null,
  //     "city": "Victoria",
  //     "state": "BC",
  //     "countryCode": "US",
  //     "postalCode": "V8P 2P5"
  //   },
  // },
  {
    "id": "192",
    "name": "Peters Medical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.9621727,
      "longitude": -80.0096489,
    },
    "address": {
      "streetAddressLine1": "665 N Main Street,",
      "streetAddressLine2": "3rd Floor",
      "streetAddressLine3": null,
      "city": "High Point",
      "state": "NC",
      "countryCode": "US",
      "postalCode": "27260"
    },
  },
  {
    "id": "193",
    "name": "Radiant Research, Inc.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.5297417,
      "longitude": -82.6346064,
    },
    "address": {
      "streetAddressLine1": "1657 East Greenville Street",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Anderson",
      "state": "SC",
      "countryCode": "US",
      "postalCode": "29621"
    },
  },
  // {
  //   "id": "194",
  //   "name": "Rejuvaline Medical Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 25.7510947,
  //     "longitude": -80.3038511,
  //   },
  //   "address": {
  //     "streetAddressLine1": "6700 SW 21st Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Miami",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33155"
  //   },
  // },
  // {
  //   "id": "195",
  //   "name": "Royal University Hospital",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 52.165293,
  //     "longitude": -106.64635,
  //   },
  //   "address": {
  //     "streetAddressLine1": "103 Hospital Drive",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Saskatoon",
  //     "state": "SK",
  //     "countryCode": "CA",
  //     "postalCode": "S7N 0W8"
  //   },
  // },
  {
    "id": "196",
    "name": "Rutgers Robert Wood Johnson Medical School",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.4945981,
      "longitude": -74.4482804,
    },
    "address": {
      "streetAddressLine1": "125  Paterson Street East Tower  8th Floor",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "New Brunswick",
      "state": "NJ",
      "countryCode": "US",
      "postalCode": "08901"
    },
  },
  // {
  //   "id": "197",
  //   "name": "Santa Maria Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.200308,
  //     "longitude": -119.178551,
  //   },
  //   "address": {
  //     "streetAddressLine1": "300 SOUTH A STREET,",
  //     "streetAddressLine2": "93030",
  //     "streetAddressLine3": null,
  //     "city": "Oxnard",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "93030"
  //   },
  // },
  {
    "id": "198",
    "name": "SDG Clinical Research, Inc",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.750542,
      "longitude": -117.16195,
    },
    "address": {
      "streetAddressLine1": "4060 Fourth Avenue",
      "streetAddressLine2": "Suite 240",
      "streetAddressLine3": null,
      "city": "San Diego",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92103"
    },
  },
  {
    "id": "199",
    "name": "Smart Medical Research Inc.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.7626091,
      "longitude": -73.8267626,
    },
    "address": {
      "streetAddressLine1": "7013 37th Ave",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Jackson Heights",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "11372"
    },
  },
  // {
  //   "id": "200",
  //   "name": "Smart Medical Research Inc.",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.72283935546875,
  //     "longitude": -73.75689697265625,
  //   },
  //   "address": {
  //     "streetAddressLine1": "207- 07 Hillside Ave",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Queens Village",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "11427"
  //   },
  // },
  {
    "id": "201",
    "name": "Southern California Research Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.6982126,
      "longitude": -117.1724492,
    },
    "address": {
      "streetAddressLine1": "131 Orange Ave.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Coronado",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92118"
    },
  },
  {
    "id": "202",
    "name": "Southern Star Research Institute",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti, DUET UC",
    "coordinates": {
      "latitude": 29.5127341,
      "longitude": -98.5915229,
    },
    "address": {
      "streetAddressLine1": "2833 Babcock Rd.,",
      "streetAddressLine2": "Suite 206",
      "streetAddressLine3": null,
      "city": "San Antonio",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "78229"
    },
  },
  {
    "id": "203",
    "name": "Southern Star Research Institute, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.8087688,
      "longitude": -84.3987279,
    },
    "address": {
      "streetAddressLine1": "77 Collier Road",
      "streetAddressLine2": "Suite 2080",
      "streetAddressLine3": null,
      "city": "Atlanta",
      "state": "GA",
      "countryCode": "US",
      "postalCode": "30309"
    },
  },
  // {
  //   "id": "204",
  //   "name": "St.-Jerome Medical Research",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.767118,
  //     "longitude": -73.999746,
  //   },
  //   "address": {
  //     "streetAddressLine1": "290 Montigny",
  //     "streetAddressLine2": "Block F",
  //     "streetAddressLine3": null,
  //     "city": "Saint-Jerome",
  //     "state": "QB",
  //     "countryCode": "US",
  //     "postalCode": "J7Z 5T3"
  //   },
  // },
  // {
  //   "id": "205",
  //   "name": "Sun Research Institute",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 29.432637,
  //     "longitude": -98.4805878,
  //   },
  //   "address": {
  //     "streetAddressLine1": "427 9th Street",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "San Antonio",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "78215"
  //   },
  // },
  // {
  //   "id": "206",
  //   "name": "SUNY Downstate Health Science University",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 40.6553702,
  //     "longitude": -73.9450773,
  //   },
  //   "address": {
  //     "streetAddressLine1": "450 Clarkson Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Brooklyn",
  //     "state": "NY",
  //     "countryCode": "US",
  //     "postalCode": "11203"
  //   },
  // },
  {
    "id": "207",
    "name": "Susquehanna Research Group, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.3288578,
      "longitude": -76.8504627,
    },
    "address": {
      "streetAddressLine1": "4387 Sturbridge Dr",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Harrisburg",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "17110"
    },
  },
  {
    "id": "208",
    "name": "Swedish Research",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC",
    "coordinates": {
      "latitude": 47.6086804,
      "longitude": -122.3226621,
    },
    "address": {
      "streetAddressLine1": "747 Broadway; Heath Building,",
      "streetAddressLine2": "Suite 725",
      "streetAddressLine3": null,
      "city": "Seattle",
      "state": "WA",
      "countryCode": "US",
      "postalCode": "98122"
    },
  },
  {
    "id": "209",
    "name": "Synergy Health",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.440679,
      "longitude": -82.457402,
    },
    "address": {
      "streetAddressLine1": "8610 State Rd 70",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Bradenton",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34208"
    },
  },
  {
    "id": "210",
    "name": "Synexus Clinical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.839106,
      "longitude": -82.715912,
    },
    "address": {
      "streetAddressLine1": "6010 Park Blvd",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Pinellas",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33781"
    },
  },
  {
    "id": "211",
    "name": "Tandem Clinical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 29.8893195,
      "longitude": -90.0993384,
    },
    "address": {
      "streetAddressLine1": "1151 Barataria Blvd",
      "streetAddressLine2": "Suite 3200",
      "streetAddressLine3": null,
      "city": "Marrero",
      "state": "LA",
      "countryCode": "US",
      "postalCode": "70072"
    },
  },

  // {
  //   "id": "212",
  //   "name": "Texas Digestive Disease Consultants",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.9411487,
  //     "longitude": -97.1425309,
  //   },
  //   "address": {
  //     "streetAddressLine1": "620 E Southlake Blvd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Southlake",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "76092"
  //   },
  // },
  {
    "id": "213",
    "name": "Texas Gastroenterology Associates - Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 30.1009479,
      "longitude": -95.4354456,
    },
    "address": {
      "streetAddressLine1": "25510 Interstate 45",
      "streetAddressLine2": "Suite 101  ",
      "streetAddressLine3": null,
      "city": "Spring",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "77386"
    },
  },
  // {
  //   "id": "214",
  //   "name": "The Medical College of Wisconsin Inc.",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.04598617553711,
  //     "longitude": -88.01654815673828,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1P Pavilion,",
  //     "streetAddressLine2": "Suite 150P",
  //     "streetAddressLine3": null,
  //     "city": "Milwuakee",
  //     "state": "WI",
  //     "countryCode": "US",
  //     "postalCode": "53226"
  //   },
  // },
  {
    "id": "215",
    "name": "The Oklahoma University Health Sciences Center - GI",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.491923,
      "longitude": -97.466819,
    },
    "address": {
      "streetAddressLine1": "1200 Everett Drive  Room 5E219",
      "streetAddressLine2": "Room 5E219",
      "streetAddressLine3": null,
      "city": "Oklahoma City",
      "state": "OK",
      "countryCode": "US",
      "postalCode": "73104"
    },
  },
  // {
  //   "id": "216",
  //   "name": "The Oregon Clinic at Gateway Medical Office",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.5302905,
  //     "longitude": -122.5622093,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1111 NE 99th Ave,SUITE 301",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Portland",
  //     "state": "OR",
  //     "countryCode": "US",
  //     "postalCode": "97220"
  //   },
  // },
  // {
  //   "id": "217",
  //   "name": "TLC Clinical Research Inc - Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.0736589,
  //     "longitude": -118.3803894,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8631 W 3rd Steet",
  //     "streetAddressLine2": "Suite 815E",
  //     "streetAddressLine3": null,
  //     "city": "Los Angeles",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90048"
  //   },
  // },
  {
    "id": "218",
    "name": "Total Gastroenterology",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "Graviti, DUET UC",
    "coordinates": {
      "latitude": 27.440679,
      "longitude": -82.457402,
    },
    "address": {
      "streetAddressLine1": "8610 E. State Rd 70",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Bradenton",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34202"
    },
  },
  {
    "id": "219",
    "name": "Total Gastroenterology, P.A.",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.4982994,
      "longitude": -82.5603775,
    },
    "address": {
      "streetAddressLine1": "300 Riverside Dr East",
      "streetAddressLine2": "Suite 1200",
      "streetAddressLine3": null,
      "city": "Bradenton",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34208"
    },
  },
  // {
  //   "id": "220",
  //   "name": "Treasure Valley Medical Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 43.6137224,
  //     "longitude": -116.2531909,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1000 North Curtis Road",
  //     "streetAddressLine2": "Suite 303/100",
  //     "streetAddressLine3": null,
  //     "city": "Boise",
  //     "state": "ID",
  //     "countryCode": "US",
  //     "postalCode": "83706"
  //   },
  // },
  {
    "id": "221",
    "name": "Tri-State Gastroenterology Associates",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti, DUET UC",
    "coordinates": {
      "latitude": 39.0225693,
      "longitude": -84.5607232,
    },
    "address": {
      "streetAddressLine1": "425 Centre View Boulevard",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Crestview Hills",
      "state": "KY",
      "countryCode": "US",
      "postalCode": "41017"
    },
  },
  {
    "id": "222",
    "name": "Tyler Research Institute",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.3310,
      "longitude": -95.29305,
    },
    "address": {
      "streetAddressLine1": "1720 S. Beckham ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Tyler",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "75701"
    },
  },

  // {
  //   "id": "224",
  //   "name": "UCLA - Gastroenterology",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.059360,
  //     "longitude": -118.443620,
  //   },
  //   "address": {
  //     "streetAddressLine1": "10889 Wilshire Blvd",
  //     "streetAddressLine2": "Suite 830",
  //     "streetAddressLine3": null,
  //     "city": "Los Angeles",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "90095"
  //   },
  // },
  // {
  //   "id": "225",
  //   "name": "United Medical Doctors",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.6135464,
  //     "longitude": -117.169359,
  //   },
  //   "address": {
  //     "streetAddressLine1": "28078 Baxter Rd",
  //     "streetAddressLine2": "Suite 530",
  //     "streetAddressLine3": "United Gastroenterologists",
  //     "city": "Murrieta",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "92563"
  //   },
  // },
  // {
  //   "id": "226",
  //   "name": "Univ. of KS School of Med. wichita Dept. of Psychiatry",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 37.701139,
  //     "longitude": -97.315221,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1010 North Kansas Street",
  //     "streetAddressLine2": "Dept P",
  //     "streetAddressLine3": null,
  //     "city": "Wichita",
  //     "state": "KS",
  //     "countryCode": "US",
  //     "postalCode": "67214"
  //   },
  // },
  {
    "id": "227",
    "name": "Univercity of Rochester Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 43.1225119,
      "longitude": -77.6261525,
    },
    "address": {
      "streetAddressLine1": "601 Elmwood Avenue",
      "streetAddressLine2": "Box 646  ",
      "streetAddressLine3": null,
      "city": "Rochester",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "14642"
    },
  },
  {
    "id": "228",
    "name": "University Gastroenterology ",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.8083845,
      "longitude": -71.4114781,
    },
    "address": {
      "streetAddressLine1": "33 Staniford St.",
      "streetAddressLine2": "2nd Fl",
      "streetAddressLine3": null,
      "city": "Providence",
      "state": "RI",
      "countryCode": "US",
      "postalCode": "02905"
    },
  },
  {
    "id": "229",
    "name": "University Hospitals Cleveland Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.5057921,
      "longitude": -81.6062368,
    },
    "address": {
      "streetAddressLine1": "11100 Euclid Avenue",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Cleveland",
      "state": "OH",
      "countryCode": "US",
      "postalCode": "44106"
    },
  },
  // {
  //   "id": "230",
  //   "name": "University of Alberta",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 53.524195,
  //     "longitude": -113.521702,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8540 112th Street NW",
  //     "streetAddressLine2": "Zeidler Centre  ",
  //     "streetAddressLine3": null,
  //     "city": "Edmonton",
  //     "state": "AB",
  //     "countryCode": "CA",
  //     "postalCode": "T6G 2X8"
  //   },
  // },
  {
    "id": "231",
    "name": "University of California Irvine Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.7872285,
      "longitude": -117.8884071,
    },
    "address": {
      "streetAddressLine1": "101 The City Drive South",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Orange",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92868"
    },
  },
  {
    "id": "232",
    "name": "University of California San Diego - La Jolla",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.881371,
      "longitude": -117.221347,
    },
    "address": {
      "streetAddressLine1": "9300 Campus Point Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "La Jolla",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92037"
    },
  },
  {
    "id": "233",
    "name": "University of California San Diego, La Jolla",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.8778127,
      "longitude": -117.2263271,
    },
    "address": {
      "streetAddressLine1": "ACTRI, 9452 Medical Center Dr.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "La Jolla",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "92037"
    },
  },
  {
    "id": "234",
    "name": "University of California San Francisco",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 37.7854773,
      "longitude": -122.4400976,
    },
    "address": {
      "streetAddressLine1": "1701 Divisadero St. Suite 102",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "San Francisco",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "94115"
    },
  },
  {
    "id": "235",
    "name": "University of Colorado Denver and Hospital",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.744101,
      "longitude": -104.8407726,
    },
    "address": {
      "streetAddressLine1": "12631 E 17th Ave",
      "streetAddressLine2": "Rm 7409",
      "streetAddressLine3": null,
      "city": "Aurora",
      "state": "CO",
      "countryCode": "US",
      "postalCode": "80045"
    },
  },
  // {
  //   "id": "236",
  //   "name": "University of Kansas Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.0569963,
  //     "longitude": -94.6095395,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3901 Rainbow Blvd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Kansas City",
  //     "state": "KS",
  //     "countryCode": "US",
  //     "postalCode": "66103"
  //   },
  // },
  {
    "id": "237",
    "name": "Univ. of KS School of Med.-wichita Dept. of Psychiatry",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 37.701139,
      "longitude": -97.315221,
    },
    "address": {
      "streetAddressLine1": "1010 North Kansas Street",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Wichita",
      "state": "KS",
      "countryCode": "US",
      "postalCode": "67214"
    },
  },
  {
    "id": "238",
    "name": "University of Kentucky Chandler Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.0334849,
      "longitude": -84.5057495,
    },
    "address": {
      "streetAddressLine1": "770 Rose Street, MN649",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Lexington",
      "state": "KY",
      "countryCode": "US",
      "postalCode": "40536"
    },
  },
  {
    "id": "239",
    "name": "University of Louisville, Clinical Trials Unit",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.2513036,
      "longitude": -85.7489141,
    },
    "address": {
      "streetAddressLine1": "200 Abraham Flexner Way  Jewish Hospital  ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Louisville",
      "state": "KY",
      "countryCode": "US",
      "postalCode": "40202"
    },
  },
  {
    "id": "240",
    "name": "University of Maryland",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.2885509,
      "longitude": -76.6260677,
    },
    "address": {
      "streetAddressLine1": "685 West Baltimore St,",
      "streetAddressLine2": "Suite 800",
      "streetAddressLine3": null,
      "city": "Baltimore",
      "state": "MD",
      "countryCode": "US",
      "postalCode": "21201"
    },
  },
  {
    "id": "241",
    "name": "University of Michigan",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.2821095,
      "longitude": -83.7273205,
    },
    "address": {
      "streetAddressLine1": "1500 E. Medical Center Dr",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Ann Arbor",
      "state": "MI",
      "countryCode": "US",
      "postalCode": "48109"
    },
  },
  // {
  //   "id": "242",
  //   "name": "University of North Carolina",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 35.901647,
  //     "longitude": -79.0533235,
  //   },
  //   "address": {
  //     "streetAddressLine1": "130 Mason Farm Road",
  //     "streetAddressLine2": "4162 Bioinformatics Building  ",
  //     "streetAddressLine3": null,
  //     "city": "Chapel Hill",
  //     "state": "NC",
  //     "countryCode": "US",
  //     "postalCode": "27599"
  //   },
  // },
  {
    "id": "243",
    "name": "University of Pennsylvania - Perelman School of Medicine",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 39.947831,
      "longitude": -75.193687,
    },
    "address": {
      "streetAddressLine1": "3400 Civic Center Blvd",
      "streetAddressLine2": "West Pavilion, 3rd Floor  ",
      "streetAddressLine3": null,
      "city": "Philadelphia",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "19104"
    },
  },
  {
    "id": "244",
    "name": "University of Southern California Keck School of Medicine",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.0625659,
      "longitude": -118.2029463,
    },
    "address": {
      "streetAddressLine1": "1520 San Pablo Street",
      "streetAddressLine2": "Suite 1000",
      "streetAddressLine3": null,
      "city": "Los Angeles",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "90033"
    },
  },
  // {
  //   "id": "245",
  //   "name": "University of Texas System",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 29.5048654,
  //     "longitude": -98.5761279,
  //   },
  //   "address": {
  //     "streetAddressLine1": "7703 Floyd Curl Drive",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "San Antonio",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "78229"
  //   },
  // },
  {
    "id": "246",
    "name": "University of Virginia Medical System",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.0315533,
      "longitude": -78.5033677,
    },
    "address": {
      "streetAddressLine1": "400 Brandon Ave,",
      "streetAddressLine2": "Lower Level,",
      "streetAddressLine3": "Room L027",
      "city": "Charlottesville",
      "state": "VA",
      "countryCode": "US",
      "postalCode": "22903"
    },
  },
  {
    "id": "247",
    "name": "University of Washington Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 47.6494115,
      "longitude": -122.3076383,
    },
    "address": {
      "streetAddressLine1": "1959 Northeast Pacific Street",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Seattle",
      "state": "WA",
      "countryCode": "US",
      "postalCode": "98195"
    },
  },
  // {
  //   "id": "248",
  //   "name": "UT Southwestern Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 32.8115282,
  //     "longitude": -96.8414985,
  //   },
  //   "address": {
  //     "streetAddressLine1": "5323 Harry Hines Blvd",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Dallas",
  //     "state": "TX",
  //     "countryCode": "US",
  //     "postalCode": "75390"
  //   },
  // },
  // {
  //   "id": "249",
  //   "name": "Vancouver General Hospital GI Research Unit",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 49.2611554,
  //     "longitude": -123.1243082,
  //   },
  //   "address": {
  //     "streetAddressLine1": "899 West 12th Avenue",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Vancouver",
  //     "state": "BC",
  //     "countryCode": "CA",
  //     "postalCode": "V5Z 1M9"
  //   },
  // },
  {
    "id": "250",
    "name": "Vanderbilt University Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 36.1424921,
      "longitude": -86.8001652,
    },
    "address": {
      "streetAddressLine1": "103C Medical Arts Building",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Nashville",
      "state": "TN",
      "countryCode": "US",
      "postalCode": "37212"
    },
  },
  // {
  //   "id": "251",
  //   "name": "Viable Clinical Research Corporation",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 45.07918,
  //     "longitude": -64.497242,
  //   },
  //   "address": {
  //     "streetAddressLine1": "203-100 Station Lane",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Kentville",
  //     "state": "NS",
  //     "countryCode": "CA",
  //     "postalCode": "B4N 0A3"
  //   },
  // },
  // {
  //   "id": "252",
  //   "name": "Viable Clinical Research Corporation",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 44.656369,
  //     "longitude": -63.530325,
  //   },
  //   "address": {
  //     "streetAddressLine1": "250 Baker Drive",
  //     "streetAddressLine2": "Ste 232",
  //     "streetAddressLine3": null,
  //     "city": "Dartmouth",
  //     "state": "NS",
  //     "countryCode": "CA",
  //     "postalCode": "B2W 6L4"
  //   },
  // },
  // {
  //   "id": "253",
  //   "name": "Virginia Mason Hospital & Medical Center",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 47.609271,
  //     "longitude": -122.328006,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1100 9th AVE D4-CRP    ",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Seattle",
  //     "state": "WA",
  //     "countryCode": "US",
  //     "postalCode": "98101"
  //   },
  // },
  {
    "id": "254",
    "name": "Washington University - Gastroenterology",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.6376806,
      "longitude": -90.2635516,
    },
    "address": {
      "streetAddressLine1": "4921 Parkview Place, 6C  660 S. Euclid Avenue, Campus Box 8124  ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "St Louis",
      "state": "MO",
      "countryCode": "US",
      "postalCode": "63110"
    },
  },
  // {
  //   "id": "255",
  //   "name": "WELLNESS ClinicaL Research",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 25.8105503,
  //     "longitude": -80.3264614,
  //   },
  //   "address": {
  //     "streetAddressLine1": "3900 NW 79 AVE",
  //     "streetAddressLine2": "Suite 511  ",
  //     "streetAddressLine3": null,
  //     "city": "Doral",
  //     "state": "FL",
  //     "countryCode": "US",
  //     "postalCode": "33166"
  //   },
  // },
  // {
  //   "id": "256",
  //   "name": "West Edmonton Gastroenterology Consultants",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 53.520468,
  //     "longitude": -113.588944,
  //   },
  //   "address": {
  //     "streetAddressLine1": "8708 155 Street",
  //     "streetAddressLine2": "Ste 408",
  //     "streetAddressLine3": null,
  //     "city": "Edmonton",
  //     "state": "AB",
  //     "countryCode": "CA",
  //     "postalCode": "T5R 1W2"
  //   },
  // },
  // {
  //   "id": "257",
  //   "name": "Woodholme Gastroenterology Associates",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 39.1471784,
  //     "longitude": -76.64026,
  //   },
  //   "address": {
  //     "streetAddressLine1": "802 Landmark Drive",
  //     "streetAddressLine2": "Suite 129  ",
  //     "streetAddressLine3": null,
  //     "city": "GLen Burnie",
  //     "state": "MD",
  //     "countryCode": "US",
  //     "postalCode": "21061"
  //   },
  // },
  {
    "id": "258",
    "name": "Yale University - DD",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.3054794,
      "longitude": -72.9361849,
    },
    "address": {
      "streetAddressLine1": "55 Park St. Room LL 15,",
      "streetAddressLine2": "Department of Pharmacy Services",
      "streetAddressLine3": null,
      "city": "New Haven",
      "state": "CT",
      "countryCode": "US",
      "postalCode": "06511"
    },
  },
  {
    "id": "259",
    "name": "Michigan Center of Medical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 42.519510,
      "longitude": -83.358960,
    },
    "address": {
      "streetAddressLine1": "30160 Orchard Lake Rd",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Farmington Hills",
      "state": "MI",
      "countryCode": "US",
      "postalCode": "48334"
    },
  },
  {
    "id": "260",
    "name": "A1 Clinical Research",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.722006,
      "longitude": -73.7608557,
    },
    "address": {
      "streetAddressLine1": "207 - 07 Hillside Ave",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Queens",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "11427"
    },
  },
  {
    "id": "261",
    "name": "Atlanta Gastroenterology Associates",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 32.3316734,
      "longitude": -90.1722086,
    },
    "address": {
      "streetAddressLine1": "2500 N. State St.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Jackson",
      "state": "MS",
      "countryCode": "US",
      "postalCode": "39216"
    },
  },
  {
    "id": "262",
    "name": "Care Access Research, Ogden",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 41.1827237,
      "longitude": -111.9554274,
    },
    "address": {
      "streetAddressLine1": "4403 Harrison Blvd #4410",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Ogden",
      "state": "UT",
      "countryCode": "US",
      "postalCode": "84403"
    },
  },
  // {
  //   "id": "263",
  //   "name": "Digestive Healthcare of Georgia, P.C",
  //   "phoneNumber": null,
  //   "clinicType": "upcoming",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 33.8080022,
  //     "longitude": -84.3983068,
  //   },
  //   "address": {
  //     "streetAddressLine1": "95 Collier Road NW, Suite 4085",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Atlanta",
  //     "state": "GA",
  //     "countryCode": "US",
  //     "postalCode": "30309"
  //   },
  // },
  {
    "id": "264",
    "name": "Emory University",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.7914848,
      "longitude": -84.3219518,
    },
    "address": {
      "streetAddressLine1": "1365 Clifton Road (Ne Clinic B-Suite 1200)",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Atlanta",
      "state": "GA",
      "countryCode": "US",
      "postalCode": "30322"
    },
  },
  {
    "id": "265",
    "name": "Gastro Florida",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 27.8952133,
      "longitude": -82.6759741,
    },
    "address": {
      "streetAddressLine1": "3001 Executive Dr., Suite 130",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Clearwater",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33762"
    },
  },
  {
    "id": "266",
    "name": "New Hope Research Development in Southern California",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.0708479,
      "longitude": -117.946838,
    },
    "address": {
      "streetAddressLine1": "741 South Orange Avenue",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "West Covina",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "91790"
    },
  },
  {
    "id": "267",
    "name": "Northwell Health",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.7815648,
      "longitude": -73.7181023,
    },
    "address": {
      "streetAddressLine1": "600 Northern Blvd., Suite 111",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Great Neck",
      "state": "NY",
      "countryCode": "US",
      "postalCode": "11021"
    },
  },
  {
    "id": "268",
    "name": "Nova Clinical Research Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 28.0776372,
      "longitude": -82.4278338,
    },
    "address": {
      "streetAddressLine1": "14486 University Cove PI",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Tampa",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33613"
    },
  },
  {
    "id": "269",
    "name": "Ochsner Medical Center",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 29.9622148,
      "longitude": -90.147946,
    },
    "address": {
      "streetAddressLine1": "1514 Jefferson Highway",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "New Orleans",
      "state": "LA",
      "countryCode": "US",
      "postalCode": "70121"
    },
  },
  // {
  //   "id": "270",
  //   "name": "Om Research LLC",
  //   "phoneNumber": null,
  //   "clinicType": "graviti",
  //   "clinicTypes": null,
  //   "coordinates": {
  //     "latitude": 34.7645777,
  //     "longitude": -118.1153261,
  //   },
  //   "address": {
  //     "streetAddressLine1": "1523 West Ave., Suite 7",
  //     "streetAddressLine2": null,
  //     "streetAddressLine3": null,
  //     "city": "Lancaster",
  //     "state": "CA",
  //     "countryCode": "US",
  //     "postalCode": "93534"
  //   },
  // },
  {
    "id": "271",
    "name": "Om Research LLC",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 34.686641,
      "longitude": -118.1631063,
    },
    "address": {
      "streetAddressLine1": "44215 15th Street West, Suite 203",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Lancaster",
      "state": "CA",
      "countryCode": "US",
      "postalCode": "93534"
    },
  },
  {
    "id": "272",
    "name": "Orlando Health",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 28.5266047,
      "longitude": -81.3793279,
    },
    "address": {
      "streetAddressLine1": "22 W. Underwood Street, 2nd Floor",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Orlando",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "32806"
    },
  },
  {
    "id": "273",
    "name": "Peak Gastroenterology Associates",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 38.8741397,
      "longitude": -104.8253511,
    },
    "address": {
      "streetAddressLine1": "2920 N Cascade Ave (3rd Floor)",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Colorado Springs",
      "state": "CO",
      "countryCode": "US",
      "postalCode": "80907"
    },
  },
  {
    "id": "274",
    "name": "Research Solutions of Arizona",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 33.4870419,
      "longitude": -112.3531959,
    },
    "address": {
      "streetAddressLine1": "13575 W Indian School Road, Suite 200",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Litchfield Park",
      "state": "AZ",
      "countryCode": "US",
      "postalCode": "85340"
    },
  },
  {
    "id": "275",
    "name": "UT Health Science Center at San Antonio",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 29.5058407,
      "longitude": -98.5777269,
    },
    "address": {
      "streetAddressLine1": "7703 Floyd Curl Dr.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "San Antonio",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "78229"
    },
  },
  {
    "id": "276",
    "name": "Victoria Gastroenterology",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 29.6557563,
      "longitude": -95.6869588,
    },
    "address": {
      "streetAddressLine1": "10818 Clovenstone Path",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Richmond",
      "state": "TX",
      "countryCode": "US",
      "postalCode": "77407"
    },
  },
  {
    "id": "277",
    "name": "Vista Health Research, LLC",
    "phoneNumber": null,
    "clinicType": "upcoming",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 25.6853697,
      "longitude": -80.384364,
    },
    "address": {
      "streetAddressLine1": "11440 N Kendall Drive",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Miami",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33176"
    },
  },
  {
    "id": "278",
    "name": "Susquehanna Research Group, LLC",
    "phoneNumber": null,
    "clinicType": "duet-cd",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 40.3288779,
      "longitude": -76.8504289,
    },
    "address": {
      "streetAddressLine1": "4387 Sturbridge Dr.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Harrisburg",
      "state": "PA",
      "countryCode": "US",
      "postalCode": "17110"
    },
  },
  {
    "id": "279",
    "name": "Ascension Medical Group Via Christi P.A.",
    "phoneNumber": null,
    "clinicType": "duet-cd",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 37.7190434,
      "longitude": -97.2001598,
    },
    "address": {
      "streetAddressLine1": "1947 N. Founders Circle",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Wichita",
      "state": "KS",
      "countryCode": "US",
      "postalCode": "67206"
    },
  },
  {
    "id": "280",
    "name": "GI Alliance",
    "phoneNumber": null,
    "clinicType": "duet-cd",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 47.6215725,
      "longitude": -122.1889747,
    },
    "address": {
      "streetAddressLine1": "1135 116th Ave NE, # LL-160",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Bellevue",
      "state": "WA",
      "countryCode": "US",
      "postalCode": "98004"
    },
  },
  {
    "id": "281",
    "name": "Care Access [Kinston]",
    "phoneNumber": null,
    "clinicType": "duet-uc",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 35.2934508,
      "longitude": -77.5813693,
    },
    "address": {
      "streetAddressLine1": "2541 N. Queen St. ",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Kinston",
      "state": "NC",
      "countryCode": "US",
      "postalCode": "28501"
    },
  },
  {
    "id": "282",
    "name": "I.H.S. Health. LLC",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 28.3012231,
      "longitude": -81.4107387,
    },
    "address": {
      "streetAddressLine1": "445 W Oak St.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Kissimmee",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "34741"
    },
  },
  {
    "id": "283",
    "name": "Columbus Clinical Services LLC",
    "phoneNumber": null,
    "clinicType": "astro",
    "clinicTypes": null,
    "coordinates": {
      "latitude": 25.7741681,
      "longitude": -80.2416173,
    },
    "address": {
      "streetAddressLine1": "110 NW 27 Ave.",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Miami",
      "state": "FL",
      "countryCode": "US",
      "postalCode": "33125"
    },
  },
  {
    "id": "284",
    "name": "Care Access Research, Salt Lake City ",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, Graviti",
    "coordinates": {
      "latitude": 40.6861119,
      "longitude": -111.8590897,
    },
    "address": {
      "streetAddressLine1": "1250 E 3900 South Suite 300",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Salt Lake City",
      "state": "UT",
      "countryCode": "US",
      "postalCode": "84124"
    },
  },
  {
    "id": "285",
    "name": "Care Access",
    "phoneNumber": null,
    "clinicType": "multiple",
    "clinicTypes": "DUET CD, DUET UC",
    "coordinates": {
      "latitude": 36.0713406,
      "longitude": -115.296643,
    },
    "address": {
      "streetAddressLine1": "9260 West Sunset Rd, Suite 306",
      "streetAddressLine2": null,
      "streetAddressLine3": null,
      "city": "Las Vegas",
      "state": "NV",
      "countryCode": "US",
      "postalCode": "89148"
    },
  },
]

function initMap() {
  var center = { lat: 41.8781, lng: -87.6298 };
  map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: center
  });

  infoWindow = new google.maps.InfoWindow();
  searchLocations(false);
}

function checkZip(value) {
  return (/(^\d{5}$)|(^\d{5}-\d{4}$)|(^\d{3}-\d{3}$)/).test(value);
}

function resetSearch() {
  var elements = document.querySelectorAll(".error");
  for (var element of elements) {
    element.classList.remove('error');
  }

  document.getElementById('zip-code-input').value = "";
  document.getElementById('search-radius-input').value = "";
  document.getElementById('study-types-input').value = "*";
  searchLocations(false);
}

function searchLocations(validate) {
  var zipCode = document.getElementById('zip-code-input').value;
  var radius = document.getElementById('search-radius-input').value;
  var studyType = document.getElementById('study-types-input').value;
  var error = false;
  var geocoder = new google.maps.Geocoder();
  var foundLocations = [];
  var searchPoint = null;

  var elements = document.querySelectorAll(".error");
  for (var element of elements) {
    element.classList.remove('error');
  }

  //validate form fields
  if (validate) {
    // if (!checkZip(zipCode)) {
    //   console.log('here')
    //   document.getElementById('zip-code-input').parentElement.classList.add("error");
    //   error = true;
    // }
    if (!zipCode) {
      console.log('here')
      document.getElementById('zip-code-input').parentElement.classList.add("error");
      error = true;
    }
    if (radius == "") {
      document.getElementById('search-radius-input').parentElement.classList.add("error");
      error = true;
    }
    if (error) return false;
  }

  //find locations within radius
  if (zipCode && radius < 999) {
    geocoder.geocode({ 'address': zipCode }, function (results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        searchPoint = new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng());

        for (var location of locations) {
          var locationPoint = new google.maps.LatLng(location.coordinates.latitude, location.coordinates.longitude);

          //get distance between points in miles
          var distance = google.maps.geometry.spherical.computeDistanceBetween(searchPoint, locationPoint) * 0.000621371;

          if (distance < radius) {
            if (location.clinicType == studyType || studyType == "*") {
              foundLocations.push(location);
            }
          }
        }
        clearLocations();

        if (foundLocations.length == 0) {
          const btnMapPopup = document.getElementById("button-close-map-popup");
          const mapPopup = document.getElementById("map-popup");
          mapPopup.style.display = "block";
          btnMapPopup.addEventListener("click", function (e) {
            mapPopup.style.display = "none";
          });
        }
        showStoresMarkers(foundLocations);

      } else {
        console.log('Geocode was not successful for the following reason: ');
        console.log(status);
      }
    });

  } else {

    // Add all locations that match the selected study type
    for (const location of locations) {

      if (location.clinicType == studyType || studyType == "*") {
        foundLocations.push(location);
      }

    }

    // No matches for the selected type were found, so show all of the locations.
    if (foundLocations.length === 0) {
      foundLocations = locations;
    }

    clearLocations();
    showStoresMarkers(foundLocations);
  }
}

function clearLocations() {
  infoWindow.close();
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(null);
  }
  markers.length = 0;
}

function showStoresMarkers(locations) {
  var bounds = new google.maps.LatLngBounds();
  for (var [index, location] of locations.entries()) {
    var latlng = new google.maps.LatLng(
      location["coordinates"]["latitude"],
      location["coordinates"]["longitude"]);

    var name = location["name"];
    var type = location['clinicType'];

    var address = location['address']['streetAddressLine1'];
    if (location['address']['streetAddressLine2'])
      address += "<br>" + location['address']['streetAddressLine2']
    if (location['address']['streetAddressLine3'])
      address += "<br>" + location['address']['streetAddressLine3']

    address += "<br>" + location['address']['city'] + ", " + location['address']['state'] + " " + location['address']['postalCode'];

    if (location['clinicTypes']) {
      address += "<div style='position:absolute;top:12px;left:12px;'><strong>Participating in: </strong>" + location['clinicTypes'] + "</div>";
    } else {
      address += "<div style='position:absolute;top:12px;left:12px;'><strong>Participating in: </strong><span style='text-transform:uppercase;'>" + location['clinicType'] + "</span></div>";
    }


    var phoneNumber = location["phoneNumber"];


    bounds.extend(latlng);
    createMarker(type, latlng, name, address, phoneNumber, index + 1)
  }

  map.setCenter(bounds.getCenter());
  map.fitBounds(bounds);

}

function createMarker(clinicType, latlng, name, address1, phoneNumber, index) {
  var icon = null;
  switch (clinicType) {
    case "duet-cd":
      icon = 'images/map-marker-tile.svg';
      break;
    case "duet-uc":
      icon = 'images/map-marker-light-blue.svg';
      break;
    case "graviti":
      icon = 'images/map-marker-green.svg';
      break;
    case "astro":
      icon = 'images/map-marker-blue.svg';
      break;
    case "chrysalis-2":
      icon = 'images/map-marker-chrysalis-2.svg';
      break;
    case "multiple":
      icon = 'images/map-marker-papillon.svg';
      break;
    case "upcoming":
      icon = 'images/map-marker-upcoming.svg';
      break;
    default:
      icon = null;
  }

  var markerIcon = {
    url: icon,
    scaledSize: new google.maps.Size(40, 40), // scaled size
  };

  var link = "https://www.google.com/maps?q=" + latlng.lat() + "," + latlng.lng();

  var html = `
      <div class="location-info-window" style="width:240px;padding-bottom:6px;">
        <div class="location-info-name" style="padding-top:32px;">
          ${name}
        </div>
        <div class="location-info-address">
          ${address1}
        </div>
        <div class="location-info-phone">
          ${phoneNumber}
        </div>
        <div class="location-info-directions">
          <a href="${link}" target="blank">Directions</a>
        </div>
      </div>
  `
  var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    icon: markerIcon,
    //label: index.toString(),
  });
  google.maps.event.addListener(marker, 'click', function () {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });
  markers.push(marker);
}

document.addEventListener("DOMContentLoaded", function (event) {
  var searchBtn = document.getElementById("searchButton");
  searchBtn.addEventListener('click', function () {
    searchLocations(true);
  });

  var resetBtn = document.getElementById("resetButton");
  resetBtn.addEventListener('click', function () {
    resetSearch();
  });

  searchLocations(false);

});