FROM nginx:1.21.0-alpine

WORKDIR /usr/share/nginx/html
COPY . /usr/share/nginx/html

# Prep
RUN cp .k8s/default.conf /etc/nginx/conf.d/ \
  && cp .k8s/.htpasswd /etc/nginx/conf.d/ \
  && rm -rf .k8s \
  && rm -rf .github \
  && rm -rf .dockerignore \
  && rm -rf Dockerfile
